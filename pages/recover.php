<?php
if (!defined('GD')) die('This file cannot be accessed directly');
if ($loggedIn) {
	echo 'Du er allerede logget ind.';
}else{
	echo '
<h2>Gendan bruger</h2>
<div class="alert alert-success" id="alert" style="display: none">
        Tjek din mail for login informationer. :-)
</div>

<form role="form" method="POST" action="'.$_SERVER['PHP_SELF'].'?page=recoveryDo">
        <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" placeholder="John@Doe.com" required>
        </div>

        <input type="submit" class="button" id="recover" value="Gendan" />
</form>';
}
?>