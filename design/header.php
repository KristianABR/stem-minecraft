<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Stem Minecraft</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/stemminecraft.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
	<script src="js/tinymce/tinymce.min.js"></script>
<script>
        tinymce.init({
        mode : "specific_textareas",
        editor_selector : "tinymce",
        theme: "modern",
        skin: 'light',
        menubar : false,
        language: 'da',
        plugins: "image link preview code hr save fullpage searchreplace",
        save_enablewhendirty: true,
        toolbar:"paste undo redo | bold italic strikethrough hr| image link | preview"});
</script>
    <script src="js/vendor/modernizr.js"></script>
    <script type="text/javascript" charset="utf-8">
							function select(category) {
								$('span#' + category).toggleClass(function() {
									$('input[value=' + category + ']').trigger('click');
									$('span#' + category).toggleClass("label-primary");
									return 'label-success';
								});
							};
                        </script>
  </head>
  <body>
  <div class="contain-to-grid">
  <nav class="top-bar" data-topbar data-options="is_hover: false">
    <section class="top-bar-section">
      <ul class="right">
        <li><a href="index.php"><i class="fa fa-home hidden"></i> Hjem</a></li>
        <li><a href="#" data-reveal-id="login"><i class="fa fa-sign-in hidden"></i> Log ind</a></li>
        <li><a href="profile.php"><i class="fa fa-user hidden"></i> Profil</a></li>
        <li class="has-dropdown">
          <a href="#"><i class="fa fa-gear"></i></a>
          <ul class="dropdown">
            <li><a href="#">Ændre profil</a></li>
            <li><a href="#">Skift email</a></li>
          </ul>
        </li>
      </ul>
    </section>
  </nav>
  </div>

<div id="login" class="reveal-modal" data-reveal>
<div class="small-12 medium-6 signupform columns">
    <h3 class="text-center">Login</h3>
    <form role="form" method="post" action="'.$_SERVER["PHP_SELF"].'?page=signinDo">
    <div class="small-12 columns">
      <label for="username">Brugernavn</label>
            <input type="text" class="form-control" name="username" required>
        </div>
        
        <div class="small-12 columns">
            <label for="passwd">Password</label>
            <input type="password" class="form-control" name="password" required>
        </div>
        <div class="columns">
          <input class="button small radius" type="submit" name="submit" value="Login">
        </div>
    </form>
  </div>
<div class="small-12 medium-6 signupform columns">
    <h3 class="text-center">Tilmeld</h3>
    <div class="alert alert-success radius" id="alert" style="display: none">
          <p>Der er blevet sendt en mail med informationer om, hvordan du logger ind.</p>
    </div>

<form role="form" method="post" action="'.$_SERVER["PHP_SELF"].'?page=signupDo">
             <div class="small-12 large-6 columns">
              <label for="username">Brugernavn</label>
              <input type="text" name="username" required>
          </div>
          <div class="small-12 large-6 columns">
              <label for="mcAcc">Minecraft Account Name</label>
              <input type="text" name="mcAcc">
          </div>
          <div class="small-12 large-6 columns">
              <label for="email">Email</label>
              <input type="text" name="email" required>
          </div>
          <div class="small-12 large-6 columns">
              <label for="confEmail">Bekræft Email</label>
              <input type="text" name="confEmail" required>
          </div>
          <div class="columns">
            <input class="button small radius" type="submit" name="submit" value="Tilmeld">
          </div>
        </form>
</div>
  <a class="close-reveal-modal">&#215;</a>
</div>



  <div class="small-10 large-12 row">