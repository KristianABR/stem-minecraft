<?php
require_once 'mailkit-cfg.php';

function mailkitsetup() {
	global $open_SSL_pub, $DKIM_s;

	echo "Get started with MJMailKit:<br><br>Generate a public/private key pair on any UNIX machine using:<br>openssl genrsa -out key.priv 2048<br>openssl rsa -in key.priv -out key.pub -pubout -outform PEM<br><br>For improved security you can use 4096 instead of 2048 in the first command<br><br>Then include the content of the keys in mailkit-cfg.php<br><br>";

	$pub_lines = explode("\n", $open_SSL_pub);
	$txt_record = "v=DKIM1; t=s; p=";
	foreach ($pub_lines as $pub_line)
		if (strpos($pub_line, '-----') !== 0)
			$txt_record .= $pub_line;
	$txt_record .= ";";
	echo "When you have entered the required info in the config
	you should add the following DNS Record for your domain:<br><br><b>Type:</b> TXT<br><br> <b>Name:</b> $DKIM_s._domainkey<br><br><b>Value:</b>
	$txt_record";
	echo "<br><br>You are now done configuring!<br>To send an email via php call:<br><br> sendMail(\$To, \$SenderEmail, \$Subject, \$Message);";
}

function DKIMQuotedPrintable($txt) {
	$tmp = "";
	$line = "";
	for ($i = 0; $i < strlen($txt); $i++) {
		$ord = ord($txt[$i]);
		if (((0x21 <= $ord) && ($ord <= 0x3A)) || $ord == 0x3C || ((0x3E <= $ord) && ($ord <= 0x7E)))
			$line .= $txt[$i];
		else
			$line .= "=" . sprintf("%02X", $ord);
	}
	return $line;
}

function DKIMBlackMagic($s) {
	global $open_SSL_priv;
	if (openssl_sign($s, $signature, $open_SSL_priv))
		return base64_encode($signature);
	else
		die("Cannot sign");
}

function NiceDump($what, $body) {
	print("After canonicalization ($what):\n");
	for ($i = 0; $i < strlen($body); $i++)
		if ($body[$i] == "\r")
			print("'OD'");
		elseif ($body[$i] == "\n")
			print("'OA'\n");
		elseif ($body[$i] == "\t")
			print("'09'");
		elseif ($body[$i] == " ")
			print("'20'");
		else
			print($body[$i]);
	print("\n------\n");
}

function SimpleHeaderCanonicalization($s) {
	return $s;
}

function RelaxedHeaderCanonicalization($s) {

	$s = preg_replace("/\r\n\s+/", " ", $s);

	$lines = explode("\r\n", $s);
	foreach ($lines as $key => $line) {
		list($heading, $value) = explode(":", $line, 2);
		$heading = strtolower($heading);
		$value = preg_replace("/\s+/", " ", $value);
		$lines[$key] = $heading . ":" . trim($value);
	}
	$s = implode("\r\n", $lines);
	return $s;
}

function SimpleBodyCanonicalization($body) {
	if ($body == '')
		return "\r\n";
	$body = str_replace("\r\n", "\n", $body);
	$body = str_replace("\n", "\r\n", $body);
	while (substr($body, strlen($body) - 4, 4) == "\r\n\r\n")
		$body = substr($body, 0, strlen($body) - 2);
	return $body;
}

function AddDKIM($headers_line, $subject, $body) {
	global $DKIM_s, $DKIM_d, $DKIM_i;

	$DKIM_a = 'rsa-sha1';
	$DKIM_c = 'relaxed/relaxed';
	$DKIM_q = 'dns/txt';
	$DKIM_t = time();
	$subject_header = "Subject: $subject";
	$headers = explode("\r\n", $headers_line);
	foreach ($headers as $header)
		if (strpos($header, 'From:') === 0)
			$from_header = $header;
		elseif (strpos($header, 'To:') === 0)
			$to_header = $header;
	$from = str_replace('|', '=7C', DKIMQuotedPrintable($from_header));
	$to = str_replace('|', '=7C', DKIMQuotedPrintable($to_header));
	$subject = str_replace('|', '=7C', DKIMQuotedPrintable($subject_header));
	$body = SimpleBodyCanonicalization($body);
	$DKIM_l = strlen($body);
	$DKIM_bh = base64_encode(pack("H*", sha1($body)));
	$i_part = ($DKIM_i == '') ? '' : " i=$DKIM_i;";
	$b = '';
	$dkim = "DKIM-Signature: v=1; a=$DKIM_a; q=$DKIM_q; l=$DKIM_l; s=$DKIM_s;\r\n" . "\tt=$DKIM_t; c=$DKIM_c;\r\n" . "\th=From:To:Subject;\r\n" . "\td=$DKIM_d;$i_part\r\n" . "\tz=$from\r\n" . "\t|$to\r\n" . "\t|$subject;\r\n" . "\tbh=$DKIM_bh;\r\n" . "\tb=";
	$to_be_signed = RelaxedHeaderCanonicalization("$from_header\r\n$to_header\r\n$subject_header\r\n$dkim");
	$b = DKIMBlackMagic($to_be_signed);
	return "X-DKIM: primaservers.com\r\n" . $dkim . $b . "\r\n";
}

function sendMail($to, $sender, $subject, $body) {
	
	$DKIM_d='primaservers.com';  
	$senderName='Prima Servers Vote';
	$headers = "From: \"$senderName\" <$sender>\r\n" . "To: $to\r\n" . "Reply-To: $sender\r\n" . "Content-Type: text/html; charset=UTF-8\r\n" . "MIME-Version: 1.0\r\n" . "Mailed-by: $DKIM_d\r\n" . "Signed-by: $DKIM_d";
	$headers = AddDKIM($headers, $subject, $body) . $headers;

	$headers = str_replace("\r\n", "\n", $headers);

	$result = mail($to, $subject, $body, $headers, "-f $sender");
	if (!$result)
		die("Cannot send email to $to");
	else {
	}
}
?>