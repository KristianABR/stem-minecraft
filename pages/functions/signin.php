<?php
if (!defined('GD'))
	die('This file cannot be accessed directly');
$nameErr = $username = $passwdErr = $passwd = $user = $uToken = $password = "";
if ($loggedIn) {
	echo 'Du er allerede logget ind.';
} else {

	require_once DIR_CORE . 'user.php';
	//Checks if the inputs is allowed
	if ($_SERVER["REQUEST_METHOD"] === "POST") {
		if (empty($_POST["username"])) {$nameErr = "Du skal skrive et brugernavn.";
		} else {
			$username = user::valInput($_POST["username"]);
			// check if name only contains letters and whitespace
			if (!preg_match("/^[a-zA-Z0-9 ]*$/", $username)) {
				$nameErr = "Kun bogstaver er tilladt.";
			}
		}

		if (empty($_POST["password"])) {$passwdErr = "Du skal skrive et password.";
		} else {
			$passwd = user::valInput($_POST["password"]);
			// check if e-mail address syntax is valid
			if (!preg_match("/^[a-zA-Z0-9 ]*$/", $passwd)) {
				$passwdErr = "Kun bogstaver er tilladt.";
			}
		}
	}
	$passwdErr = $nameErr = "";
	//Echo a error message if input is illegal
	if (!empty($nameErr) || !empty($passwdErr)) {
		echo $nameErr . '<br>' . $passwdErr;
		exit();
	} else {

		$results = $db -> where('username', $username) -> get('users');

		if (!isset($results['0'])) {
			$user = "";
			echo "Brugernavnet findes ikke i vores database.";
		} else {
			// contains array of returned rows

			$user = $results['0'];
			$password = hash("sha512", $passwd . $user['token']);

			//Echoing all data.... Debugging
			/*echo '<br>Username: ' . $username;
			echo '<br>Input Passwd: ' . $passwd;
			echo '<br>Password: ' . $password;
			echo '<br>Token: ' . $user['token'];
			echo '<br>';*/

			if ($password === $user['password']) {
				echo 'Korrekt kode';
				session_destroy();
				session_start();

				$_SESSION['id'] = $user['id'];
				$_SESSION['pass'] = $password;
				$_SESSION['username'] = $username;
				$_SESSION['rank'] = $user['rank'];

				header("Location: " . DOMAIN);
			} else {
				echo 'Forkert kode';
			}
		}
	}
}
?>