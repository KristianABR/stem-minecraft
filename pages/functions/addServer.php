<?php
if (!defined('GD'))
	die('This file cannot be accessed directly');

if ($loggedIn) {
	require_once (DIR_CORE . 'user.php');

	$nameErr = $addrErr = $portErr = $descriptionErr = $websiteErr = NULL;

	//Checks if the inputs is allowed
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_POST["servername"])) {$nameErr = "Du skal skrive et servernavn.";
		} else {
			$servername = user::valInput($_POST["servername"]);
		}

		if (empty($_POST["ipadress"])) {$addrErr = "Du skal skrive en ip adresse på din server.";
		} else {
			$addr = user::valInput($_POST["ipadress"]);
		}

		if (empty($_POST["port"])) {$addrErr = "Du skal skrive din server port.";
		} else {
			$port = user::valInput($_POST["port"]);
			// check if port syntax is valid
			if (!preg_match('/^\d{4,6}$/', $port)) {
				$portErr = "Forkert port.";
			}
		}

		if (empty($_POST["description"])) {$addrErr = "Du skal skrive en server beskrivelse på mindst 500 bogstaver.";
		} else {
			$description = user::valInput($_POST["description"]);
			if (strlen($description) < 400)
				$descriptionErr = 'Din beskrivelse overholder ikke kravet om 400 bogstaver.';
		}

		if (isset($_POST["website"])) {
			$website = user::valInput($_POST["website"]);
			// check if port syntax is valid
			if (!empty($website) && !preg_match('^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$^', $website)) {
				$websiteErr = "Forkert hjemmeside format.";
			}
		}

		echo $nameErr . '<br>' . $addrErr . '<br>' . $portErr . '<br>' . $descriptionErr . '<br>' . $websiteErr;

		//Counting rows in our table users for setting a ID
		$results = $db -> query('SELECT COUNT(*) AS count FROM servers');
		$count = $results['0'];
		//End counting rows, and plussing by 1, for getting a new id.
		$id = $count['count'] + 1;

		$votes = 0;

		$db -> where('id', $_SESSION['id']);
		$results = $db -> get('users');

		$user = $results[0]['username'];

		//Echoing all data.... Debugging
		/*
		 echo 'Servername: ' . $servername;
		 echo '<br>IP: ' . $addr;
		 echo '<br>port: ' . $port;
		 echo '<br>Description: ' . $description;
		 echo '<br>Username: ' . $user;
		 echo '<br>dateSubmitted: DB Configuration!';
		 echo '<br><br>Website: ' . $website;
		 echo '<br><br>dateUpdated: DB Configuration!';
		 echo '<br><br>votes: ' . $votes;
		 echo '<br><br>id: ' . $id;

		 echo '<br>';
		 */

		$db -> where('ip', $addr);
		$db -> where('port', $port);
		$results = $db -> get('servers');

		if (empty($results) && empty($nameErr) && empty($addrErr) && empty($portErr) && empty($descriptionErr) && empty($websiteErr)) {
			$insertData = array('name' => $servername, 'ip' => $addr, 'port' => $port, 'description' => $description, 'username' => $user, 'website' => $website, 'votes' => $votes, 'id' => $id, 'rank' => $id, );

			if ($db -> insert('servers', $insertData))
				;

			require_once (DIR_3RDPARTY . 'mailKit.php');

			sendMail('noreply@primaservers.com', 'noreply@primaservers.com', 'Vote | Ny server tilføjet', 'Der er lige blevet tilføjet en ny server til vote databasen. <br> Servername: ' . $servername . '
			<br>IP: ' . $addr . '
		 <br>port: ' . $port . '
		 <br>Description: ' . $description . '
		 <br>Username: ' . $user . '
		 <br><br>Website: ' . $website . '
		 <br><br>votes: ' . $votes . '
		 <br><br>id: ' . $id);

			echo 'Din server er nu tilføjet til vores database.';
		} elseif (!empty($results)) {
			echo 'Serveren findes allerede i vores database.';
		}
	}
}
/*
name
ip
port
description
username
dateSubm
website
dateUpdate
votes
id
rank
categories
votifier
online
gnstPlayers
*/
?>