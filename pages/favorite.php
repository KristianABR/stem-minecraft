<?php

if ($loggedIn) {

	if (isset($_GET['add']) && isset($_GET['id'])) {

		if (preg_match("([0-9]+)", $_GET['id'])) {

			$db->where('id', $_GET['id']);
			$servers = $db->get('servers');
		if (!empty($servers)){

			$db -> where("id", $_SESSION['id']);
			$result = $db -> get("users");

			if (!empty($result[0]['favorite'])) {

				if (strpos($result[0]['favorite'], $_GET['id']) !== FALSE) {

					echo 'Allerede tilføjet';
					break;

				} else {

					$newFav['favorite'] = $result[0]['favorite'] . ',' . $_GET['id'];

				}

			} else {

				$newFav['favorite'] = $_GET['id'];

			}

			$db -> where('id', $_SESSION['id']);
			$db -> update('users', $newFav);

			header("Location: /server/" . $_GET['id']);
		}
		}else{
			echo 'Du skal tilføje et server id.';
		}

		} elseif( isset($_GET['del']) && isset($_GET['id']) ){

			$db->where('id', $_SESSION['id']);
			$user = $db->get('users');

			if( strpos($user[0]['favorite'], $_GET['id']) !== FALSE ){

				$fav = str_replace($_GET['id'], '', $user[0]['favorite']);

				$data['favorite'] = str_replace(',,', ',', $fav);

				$db -> where('id', $_SESSION['id']);
				$db -> update('users', $data);

				header("Location: /server/".$_GET['id']);

			}else{

				echo 'Denne server er ikke i dine favoritter.';

			}

		} else {

			echo 'Du har ikke sat nogle servere.';

		}
} else {
	echo 'Du er ikke logget ind.';
}

?>