<?php
include (DIR_BASE . '3rdparty/MysqliDb.php');

function get($Url) {

	// is cURL installed yet?
	if (!function_exists('curl_init')) {
		die('Sorry cURL is not installed!');
	}

	// OK cool - then let's create a new cURL resource handle
	$ch = curl_init();

	// Now set some options (most are optional)

	// Set URL to download
	curl_setopt($ch, CURLOPT_URL, $Url);

	// Set a referer
	curl_setopt($ch, CURLOPT_REFERER, "http://vote.primaservers.com/");

	// User agent
	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");

	// Include header in result? (0 = yes, 1 = no)
	curl_setopt($ch, CURLOPT_HEADER, 0);

	// Should cURL return or print out the data? (true = return, false = print)
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Timeout in seconds
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);

	// Download the given URL, and return output
	$output = curl_exec($ch);

	// Close the cURL resource, and free system resources
	curl_close($ch);

	return $output;
}

include (DIR_CORE . 'server.class.php');
session_start();

//Is the user logged in?
$loggedIn = !empty($_SESSION['id']) && !empty($_SESSION['pass']) && !empty($_SESSION['username']);
$isAdmin = $loggedIn && $_SESSION['rank'] >= 10;

$db = new Mysqlidb(CONFIG::db_host, CONFIG::db_user, CONFIG::db_pass, CONFIG::db_name);

//Checks if $_GET['page'] is set, and if it is, then echo the class active.
function active($page = "") {
	$getPage = "";

	if (isset($_GET['page'])) {
		$getPage = $_GET['page'];
	}

	if ($getPage == $page) {
		return 'class="active"';
	}
}

//Class for importing css and JS.
class import {
	public static function css($file) {
		echo '<link type="text/css" rel="stylesheet" href='.DIR_STYLES.$file.'>';
	}

	public static function js($file) {
		echo '<script src=' .DIR_JS . $file . '></script>';
	}

};

if (CONFIG::debug) {
	Error_Reporting( E_ALL | E_STRICT );
	Ini_Set( 'display_errors', true );
};
?>