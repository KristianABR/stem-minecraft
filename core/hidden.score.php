<?php
//ob_start();
include '../config.php';
require '../3rdparty/MinecraftServerPing.php';
require DIR_3RDPARTY . 'MinecraftQuery.class.php';

$info = $players = $plugins = $plugins = $onlinePlayers = $maxPlayers = $software = $onlinePlayersWhoA = $onlinePlayersWho = $ping = NULL;

echo '<pre>';

if (isset($_GET['id'])) {
	$db -> where('id', $_GET['id']);
	$server = $db -> get('servers');

	$ping = server::ping($server[0]['real_ip']);

		//Status
		$status = explode(',', $server[0]['status']);
		$cOffline = explode('.', $status[0]);
		$cOnline = explode('.', $status[1]);

		$uptime = ( $cOnline[1] != 0 ) ? 100*($cOnline[1] - $cOffline[1]) / $cOnline[1] : 100;

	if ($ping === "NULL") {

		$gnstPlayer = $server[0]['gnstPlayers'].',0';

		if ( $server[0]['offlineDate'] === 'null'){
			echo $offlineDate = date('dmYGis');
		}
			$of = $cOffline[1] + 1;

			$status = "0." . $of . ",1.".$cOnline[1];

		$data = array('players' => 0, 'whichPlayers' => NULL, 'online' => 0, 'ping' => NULL, 'uptime' => $uptime, 'offlineDate' => $offlineDate, 'gnstPlayers' => $gnstPlayers );

		$db -> where('id', $_GET['id']);
		$db -> update('servers', $data);
	} else {
		$Query = new MinecraftQuery();

		try {
			$Query -> Connect($server[0]['real_ip'], $server[0]['real_port']);

			$info = $Query -> GetInfo();
			$players = $Query -> GetPlayers();

		} catch( MinecraftQueryException $e ) {
			//echo $e -> getMessage();
		}

		if (isset($info)) {
			$onlinePlayers = $info['Players'];
			$maxPlayers = $info['MaxPlayers'];
			$software = $info['Software'];

			if (is_array($info['Plugins'])) {
				$plugins = implode(',', $info['Plugins']);
			}

			if (!empty($players)) {
				$onlinePlayersWhoA = array();

				foreach ($players as $player) {
					array_push($onlinePlayersWhoA, $player);
				}

				$onlinePlayersWho = implode(',', $onlinePlayersWhoA);
			}

			$ol = $cOnline[1] + 1;

			$status = "0." . $cOffline[1] . ",1.".$ol;

			$gnstPlayer = $server[0]['gnstPlayers'].','.$onlinePlayers;

			$data = array('players' => $onlinePlayers, 'maxPlayers' => $maxPlayers, 'whichPlayers' => $onlinePlayersWho, 'plugins' => $plugins, 'software' => $software, 'ping' => $ping, 'online' => 1, 'status' => $status, 'uptime' => $uptime, 'offlineDate' => 'null', 'gnstPlayers' => $gnstPlayer);
			$db -> where('id', $_GET['id']);
			$db -> update('servers', $data);
		}

		// Edit this ->
		define('MQ_SERVER_ADDR', $server[0]['real_ip']);
		define('MQ_SERVER_PORT', $server[0]['real_port']);
		define('MQ_TIMEOUT', 1);
		// Edit this <-

		// Display everything in browser, because some people can't look in logs for errors
		Error_Reporting(E_ALL | E_STRICT);
		Ini_Set('display_errors', true);

		$Info = false;
		$Query = null;

		try {
			$Query = new MinecraftPing(MQ_SERVER_ADDR, MQ_SERVER_PORT, MQ_TIMEOUT);

			$Info = $Query -> Query();

			if ($Info === false) {
				$Query -> Close();
				$Query -> Connect();

				$Info = $Query -> QueryOldPre17();
			}
		} catch( MinecraftPingException $e ) {
			$Exception = $e;
		}

		if ($Query !== null) {
			$Query -> Close();
		}

		//print_r($Info);

		if (isset($Info['favicon'])) {
			$icon = $Info['favicon'];
		} else {
			$icon = '../resources/images/no-servericon.png';
		}

		$data = array('icon' => $icon);
		$db -> where('id', $_GET['id']);
		$db -> update('servers', $data);

	}


	//Rank
	$db->orderBy("score","Desc");
	$db -> orderBy("votes", "desc");
	$db -> orderBy("dateSubmitted", "Desc");

	$results = $db->get('servers');

	foreach ($results as $key => $value) {

		$gnstPlayer = array_sum(explode(',', $value['gnstPlayers'])) / count(explode(',', $value['gnstPlayers'])) + 1;

		$score = $value['uptime']*$value['votes'] / ( ( 1 / $gnstPlayer ) * 100 );


		$data = array (
    		'rank' => $key + 1,
    		'score' => $score
		);
		$db->where('id', $value['id']);
		$db->update('servers', $data); 
	}

	//uptime*votes/((1/gnstPlayer)*10^3)
} else {
	echo 'NULL';
}
//ob_flush();
?>