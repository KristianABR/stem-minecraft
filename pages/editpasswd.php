<?php
if ($loggedIn) {
    echo '
    <h2>Ændre password</h2>
    
    <form role="form" method="post" action="'.$_SERVER["PHP_SELF"].'?page=editPassDo">
            <div class="form-group">
                    <label for="oldPasswd">Nuværrende password *</label>
                    <input type="password" class="form-control" name="oldPassword" placeholder="MySecretPassword" required>
            </div>
    
            <div class="form-group">
                    <label for="newPasswd">Nye Password *</label>
                    <input type="password" class="form-control" name="newPassword" placeholder="MyNewSecretPassword" required>
            </div>
            <div class="form-group">
                    <label for="confNewPasswd">Valider nye Password *</label>
                    <input type="password" class="form-control" name="confNewPassword" placeholder="MyNewSecretPassword" required>
            </div>
    
            <input class="button" type="submit" name="submit" value="Ændre">
    </form>';

}else{
    echo 'Du er ikke logget ind på siden.';
}
?>