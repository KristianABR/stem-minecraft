function vote(id) {

  		$( 'div.ans' ).css( 'display', 'block' );
        httpGet('vote.php?id=' + id);
}
function httpGet(theUrl){
	var xmlHttp = null;
	xmlHttp = new XMLHttpRequest();
	xmlHttp.open( "GET", theUrl, false );
	xmlHttp.send( null );
	var ans = xmlHttp.responseText;
	$('div.ans').html(ans);
}
function votifier(){
	if (document.getElementById('votH').style.display === 'block') {
		$( '#votH' ).fadeIn();
		$( '#votH' ).css( 'display', 'none' );
		$('#votifierButton').val('Jeg bruger votifier');
	} else{
		$( '#votH' ).fadeOut().css( 'display', 'block' );
		$('#votifierButton').val('Jeg bruger ikke votifier');
	};
}