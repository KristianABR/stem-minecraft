<?php
if (!defined('GD'))
	die('This file cannot be accessed directly');
if ($loggedIn) {
	echo 'Du er allerede logget ind.';
} else {
	echo '
<h2>Tilmeld dig</h2>

<div class="alert alert-success" id="alert" style="display: none">
        	<p>Tjek din mail for login informationer. :-)</p>
</div>

<form role="form" method="post" action="'.$_SERVER["PHP_SELF"].'?page=signupDo">
             <div class="small-12 large-6 columns">
	            <label for="username">Brugernavn</label>
	            <input type="text" name="username" placeholder="John Doe" required>
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="mcAcc">Minecraft Account Name</label>
	            <input type="text" name="mcAcc" placeholder="Notch">
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="email">Email</label>
	            <input type="text" name="email" placeholder="john@doe.com" required>
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="confEmail">Valider Email</label>
	            <input type="text" name="confEmail" placeholder="john@doe.com" required>
	        </div>
	        <div class="columns">
        		<input class="button tiny" type="submit" name="submit" value="Opret">
        	</div>
		</form>
     </div>
<div class="text-right">Vi logger ipen: <code>' . $_SERVER["REMOTE_ADDR"] . '</code></div>';
}
