<?php
if (!defined('GD'))
	die('This file cannot be accessed directly');
if($loggedIn){
?>
<h2>Tilføj server</h2>

<form role="form" method="post" action="<?php echo $_SERVER["PHP_SELF"]?>?page=addServerDo">

        <div class="form-group">
                <label for="servername">Server Navn *</label>
                <input type="text" class="form-control" name="servername" placeholder="United Players of Denmark" required>
        </div>
        
        <div class="form-group">
                <label for="ipadress">Serverens IP adresse *</label>
                <input type="text" class="form-control" name="ipadress" placeholder="37.187.135.28" required>
        </div>
        
        <div class="form-group">
                <label for="port">Serverens Port *</label>
                <input type="text" class="form-control" name="port" placeholder="25565" required>
        </div>
    
        <div class="form-group">
                <label for="descriptopm">Server beskrivelse *</label>
                <textarea name="description" placeholder="Hej, Jeg vil gerne introduksere jer til serveren (...)" style="height: 200px;" required></textarea>
        </div>
        <div class="from-group">
                <label for="categories">Kategorier</label>
                        <input id="Survival" type="checkbox">   <label for="Survival">Survival</label>
                        <input id="Creative" type="checkbox">   <label for="Creative">Creative</label>
                        <input id="Factions" type="checkbox">   <label for="Factions">Factions</label>
                        <input id="Towny" type="checkbox">      <label for="Towny">Towny</label>
                        <input id="Skyblock" type="checkbox">   <label for="Skyblock">Skyblock</label>
                        <input id="Economy" type="checkbox">    <label for="Economy">Economy</label>
                        <input id="PvP" type="checkbox">        <label for="PvP">PvP</label>
                        <input id="PvE" type="checkbox">        <label for="PvE">PvE</label>
                        <input id="FTB" type="checkbox">        <label for="FTB">FTB</label>
                        <input id="Tekkit" type="checkbox">     <label for="Tekkit">Tekkit</label>
                        <input id="Mods" type="checkbox">       <label for="Mods">Andre mods</label>
                        <input id="Prison" type="checkbox">     <label for="Prison">Prison</label>
                        <input id="Hub" type="checkbox">        <label for="Hub">Hub</label>
                        <input id="Roleplay" type="checkbox">   <label for="Roleplay">Roleplay</label>
                        <input id="MiniGames" type="checkbox">  <label for="MiniGames">Mini Games</label>
        </div>

        <div class="form-group">
        <label for="votifier">Votifier</label>
                <input id="votifierButton" type='button' onclick="votifier()" value="Jeg bruger votifier">
                <div id="votH">
                        <label for="votifierPub">Votifier Public Key</label>
                        <textarea name="votifierPub" placeholder="Copy, paste ALT indhold i filen /plugins/Votifier/rsa/public.key." style="height: 200px;"></textarea>

                        <label for="port">Votifier Port</label>
                        <input type="text" class="form-control" name="votifierPort" placeholder="8192">
                </div>
        </div>
        <hr>
        
        <div class="form-group">
                <label for="website">Server Website</label>
                <input type="text" class="form-control" name="website" placeholder="http://UPDenmark.dk/">
        </div>

        <input class="button" type="submit" name="submit" value="Tilføj server">
</form>
<div class="text-right">Husk at sætte <code>enable-query=true</code> i din server.properties</div>


<?php
}else{
	echo 'Du er ikke logget ind.';
}
?>