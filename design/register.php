<?php include 'header.php'; ?>
	<div class="row">
		<div class="alert alert-success" id="alert" style="display: none">
        	<p>Tjek din mail for login informationer. :-)</p>
		</div>
		<h2>Tilmeld dig</h2>

		<form role="form" method="post" action="/index.php?page=signupDo">
	        <div class="small-12 large-6 columns">
	            <label for="username">Brugernavn</label>
	            <input type="text" name="username" placeholder="John Doe" required>
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="mcAcc">Minecraft Account Name</label>
	            <input type="text" name="mcAcc" placeholder="Notch">
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="email">Email</label>
	            <input type="text" name="email" placeholder="john@doe.com" required>
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="confEmail">Valider Email</label>
	            <input type="text" name="confEmail" placeholder="john@doe.com" required>
	        </div>
	        <div class="columns">
        		<input class="button tiny" type="submit" name="submit" value="Opret">
        	</div>
		</form>

	</div>
<?php include 'footer.php'; ?>