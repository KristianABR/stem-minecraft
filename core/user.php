<?php
class user {
	public $_db;

	function __constuct() {
		global $db;
		$this -> _db = $db;
	}

	public static function genRandString($length = 30) {
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}

	public static function valInput($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	public static function logout() {
		session_destroy();
		unset($_SESSION['id']);
		unset($_SESSION['pass']);
	}

	public static function login($id, $pass) {
		session_destroy();
		session_start();
		$_SESSION['id'] = $id;
		$_SESSION['pass'] = $pass;

		$x = self::find($id);
		$_SESSION['username'] = $x[0]['username'];

	}

	public static function hashing($salt, $text, $algo='sha256') {
		hash($algo, $text . $salt);
	}

	public static function show($users) {
		global $db;
		$i = 1;
		if (is_array($users)) {

			foreach ($users as $user) {
				echo '<div class="user ' . $user['id'] . '">';
				echo '<div class="user num">#' . $i . '</div>';
				$i++;
				echo '<div class="user id">ID: <a href="?page=profile&id='.$user['id'].'">' . $user['id'] . '</a></div>';
				echo '<div class="user name">Name: <a href="?page=profile&id='.$user['id'].'">' . $user['username'] . '</a></div>';
				echo '<div class="user mail">Email: <a href="mailto:'.$user['email'].'">' . $user['email'] . '</a></div>';
				echo '<div class="user ip">IP: ' . $user['IP'] . '</div>';
				echo '<div class="user rank">Rank: ' . $user['rank'] . '</div>';
				echo '<div class="user minecraftaccount">Minecraft: ' . $user['minecraftaccount'] . '</div>';
				echo '<div class="user date">Creation Date: ' . $user['creationDate'] . '</div>';

				echo '</div>';
			}
		}
	}

	public static function find($arg = '', $row = '', $respond='') {
		global $db;
		if (!empty($arg)) {
			if (is_int($arg)) {
				$db -> where('id', $arg);
				$result = $db -> get('users');

				if ($result) {
					return $result;
				} else {
					return 'Det ser ikke ud til, at vi har nogen bruger i vores database, med id\'et <i>' . $arg . '</i>.';
				}

			} elseif ($arg === 'all') {
				$db -> orderBy("id", "asc");
				$db -> orderBy("rank", "Desc");
				$db -> orderBy("username", "asc");
				$db -> orderBy("minecraftaccount", "asc");
				$x = $db -> get('users');

				self::show($x);

			} elseif (!empty($row)) {
				$db -> where($row, $arg);
				$result = $db -> get('users');

					if ($result){
						return $result[0][$respond];
					}
			}else {
				$db -> where('username', $arg);
				$result = $db -> get('users');

				if ($result) {
					$x = $result[0]['username'];
					if ($x) {
						$db->where('username', $arg);
						$db -> orderBy("id", "asc");
						$db -> orderBy("rank", "Desc");
						$db -> orderBy("username", "asc");
						$db -> orderBy("minecraftaccount", "asc");
						$x = $db -> get('users');

						user::show($x);
					}
				} else {
					return 'Det ser ikke ud til, at brugeren <i>' . $arg . '</i> findes.';
				}
			}
		} else {
			return 'Du skal skrive noget som du leder efter.';
		}
	}

}
?>