<?php
function get($Url) {

	// is cURL installed yet?
	if (!function_exists('curl_init')) {
		die('Sorry cURL is not installed!');
	}

	// OK cool - then let's create a new cURL resource handle
	$ch = curl_init();

	// Now set some options (most are optional)

	// Set URL to download
	curl_setopt($ch, CURLOPT_URL, $Url);

	// Set a referer
	curl_setopt($ch, CURLOPT_REFERER, "http://vote.primaservers.com/");

	// User agent
	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");

	// Include header in result? (0 = yes, 1 = no)
	curl_setopt($ch, CURLOPT_HEADER, 0);

	// Should cURL return or print out the data? (true = return, false = print)
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Timeout in seconds
	curl_setopt($ch, CURLOPT_TIMEOUT, 1);

	// Download the given URL, and return output
	$output = curl_exec($ch);

	// Close the cURL resource, and free system resources
	curl_close($ch);

	return $output;
}

function query($server, $method = "query") {
	if (isset($server)) {
		if (isset($method)) {
			switch ($method) {
				case 'status' :
					$res = get("http://minecraft-api.com/v1/status/?server=" . $server);
					break;

				case 'get' :
					$res = get("http://minecraft-api.com/v1/get/?server=" . $server);
					break;

				case 'players' :
					$res = get("http://minecraft-api.com/v1/players/?server=" . $server);
					break;

				case 'motd' :
					$res = get("http://minecraft-api.com/v1/motd/?server=" . $server);
					break;

				case 'batched' :
					$res = get("http://minecraft-api.com/v1/batched/?server=" . $server);
					break;

				case 'logo' :
					$res = get("http://minecraft-api.com/v1/logo/?server=" . $server);
					break;

				case 'query' :
					$res = get("http://minecraft-api.com/v1/query/?server=" . $server);
					break;

				default :
					$res = "null";
					break;
			}

			$result = json_decode($res, true);
			return $result;

		} else {
			return "null";
		}
	} else {
		return "null";
	}
}

function ping($domain) {
	$starttime = microtime(true);
	$file = fsockopen($domain, 80, $errno, $errstr, 1);
	$stoptime = microtime(true);
	$status = 0;

	if (!$file){
		$status = NULL;
		
		return 'NULL';
	// Site is down
	}else {
		fclose($file);
		$status = ($stoptime - $starttime) * 1000;
		
		return substr($status, 0, 5);
	}
}

function dnslookup($host){
		$ips = array();
		if(preg_match_all('/Address: ((?:\d{1,3}\.){3}\d{1,3})/', `nslookup $host`, $match) > 0){
		    $ips['real_ip'] = $match[1];
		}

		if (empty($ips)) {
 		   if(preg_match_all("/service = (\d{1,3}) (\d{1,3}) (\d{4,7}) (\w.{1,50})/", `nslookup -type=SRV _minecraft._tcp.$host`, $match) > 0){
		        $ips['port'] = $match[3];
		        $ips['real_ip' ] = $match[4];
					}else{
					$ips = NULL;
					}
					}

					return $ips;
					}
?>