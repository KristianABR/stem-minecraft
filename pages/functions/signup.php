<?php
if (!defined('GD'))
	die('This file cannot be accessed directly');
if ($loggedIn) {
	echo 'Du er allerede logget ind.';
} else {
	require_once (DIR_BASE . 'config.php');
	require_once (DIR_CORE . 'user.php');

	$nameErr = $emailErr = $confEmailErr = $username = $email = $confEmail = $mcAccErr = $mcAcc = "";

	//Checks if the inputs is allowed
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_POST["username"])) {$nameErr = "Du skal skrive et brugernavn.";
		} else {
			$username = user::valInput($_POST["username"]);
		}

		if (empty($_POST["email"])) {$mailErr = "Du skal skrive en mail.";
		} else {
			$email = strtolower(user::valInput($_POST["email"]));
			// check if email address syntax is valid
			if (!preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', $email)) {
				$emailErr = "Din mail passer ikke.";
			}
		}

		if (empty($_POST["confEmail"])) {$confEmailErr = "Du skal skrive en validerende email.";
		} else {
			$confEmail = strtolower(user::valInput($_POST["confEmail"]));
			// check id confemail syntax is valid
			if (!preg_match('/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', $confEmail) || $confEmail !== $email) {
				$confEmailErr = "Din validerende email passer ikke.";
			}
		}

		if (empty($_POST["mcAcc"])) {$mcAccErr = "Du skal skrive en minecraft account.";
		} else {
			$mcAcc = user::valInput($_POST["mcAcc"]);
			// check id confemail syntax is valid
			if (file_get_contents('http://minecraft.net/haspaid.jsp?user=' . $mcAcc) == 'false') {
				$mcAccErr = 'Minecraft brugeren eksistere ikke.';
			}
		}
	}

	echo $nameErr . '<br>' . $emailErr . '<br>' . $confEmailErr . '<br>' . $mcAccErr;

	$passwd = user::genRandString(7);
	$token = user::genRandString(30);
	$ip = $_SERVER['REMOTE_ADDR'];
	$password = hash('sha512', $passwd . $token);

	$results = $db -> get('users');

	$ids = array();
	$max = 0;
	foreach ($results as $key => $v) {
		if ($v['id'] > $max) {
			$max = $v['id'];
			$ids[] = $v['id'];
		}

	}
	$id = max($ids) + 1;

	//Echoing all data.... Debugging
	/*
	 echo 'Username: ' . $username;
	 echo '<br>Email: ' . $email;
	 echo '<br>ValEmail: ' . $confEmail;
	 echo '<br>Passwd: ' . $password;
	 echo '<br>Token: ' . $token;
	 echo '<br>IP: ' . $ip;
	 echo '<br><br>Uncrypted Passwd: ' . $passwd;
	 echo '<br>ID: ' . $id;
	 echo '<br>Minecraft Account: ' . $mcAcc;
	 echo '<br>';
	 */

	$db -> where('username', $username);
	$nameResults = $db -> get('users');

	$db -> where('email', $email);
	$mailResults = $db -> get('users');

	$db -> where('minecraftaccount', $mcAcc);
	$mcAccResults = $db -> get('users');

	if (empty($mailResults) && empty($nameResults) && empty($nameErr) && empty($emailErr) && empty($confEmailErr) && empty($mcAccErr) && empty($mcAccResults)) {

		$verify = hash("md5", $mcAcc . $token);
		$data = array('username' => $username, 'email' => $email, 'token' => $token, 'password' => $password, 'IP' => $ip, 'rank' => 0, 'id' => $id, 'minecraftaccount' => $mcAcc, 'verify' => $verify);

		$x = $db -> insert('users', $data);

		require_once (DIR_3RDPARTY . 'mailKit.php');

		sendMail($email, 'noreply@' . CONFIG::domain, CONFIG::site_name . ' | Velkommen', 'Velkommen til ' . CONFIG::site_name . ', ' . $username . '.
			 <br> Dit password er: ' . $passwd . '
			<br><br>Husk, at du altid kan ændre dit password i menuen, der ligger under \'Profil\'.
			<br><br> God fornøjelse. <br>Hilsner <a href="http://' . CONFIG::domain . '/">' . CONFIG::domain . '</a>.');

		echo 'Velkommen til, ' . $username . '.<br> Tjek din mail for dit password. :-)';
	} elseif (!empty($nameResults)) {
		echo 'Brugernavnet findes allerede.';
	} elseif (!empty($mailResults)) {
		echo 'Mailen findes allerede.';
	} elseif (!empty($mcAccResults)) {
		echo 'Minecraft accounten finden allerede.';
	}
}
?>