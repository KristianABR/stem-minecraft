<?php
include 'header.php';
?>
</div>
<div class="header">
    <div class="row">
        <div class="headercontainer small-4 columns">
        	<h2>Stem Minecraft</h2>
        	<p>Der er <strong>738</strong> spillere online</p>
        	<p>på <strong>245</strong> online servere</p>
        </div>
        <div class="headercontainer small-4 columns">
        	<h2>Seneste servere</h2>
        	<a href="server.php">UPDenmark</a></p>
        	<p><a href="server.php">MyLittleCreeper</a></p>
        	<p><a href="server.php">EnderCraft</a></p>
        </div>
    </div>
</div>
<div class="row">
    <div class="serversearch small-12">
        <fieldset>
            <legend>
                Find server
            </legend>
            <div class="row collapse">
                <div class="small-2 columns">
                    <span class="prefix">Navn</span>
                </div>
                <div class="small-8 columns">
                    <input type="text" class="noradius" placeholder="Søg efter server...">
                </div>
                <div class="small-2 columns">
                    <a href="#" class="button rightradius postfix">Go</a>
                </div>
                <div class="small-10 columns">
                    <span class="label radius pointer" id="towny" onclick="select('towny')">Towny</span>
                    <span class="label radius pointer" id="factions" onclick="select('factions')">Factions</span>
                    <span class="label radius pointer" id="creative" onclick="select('creative')">Creative</span>
                    <span class="label radius pointer" id="survival" onclick="select('survival')">Survival</span>
                    <span class="label radius pointer" id="vanilla" onclick="select('vanilla')">Vanilla</span>
                    <span class="label radius pointer" id="pvp" onclick="select('pvp')">PVP</span>
                    <span class="label radius pointer" id="pve" onclick="select('pve')">PVE</span>
                    <span class="label radius pointer" id="rpg" onclick="select('rpg')">RPG</span>
                    <span class="label radius pointer" id="ftb" onclick="select('ftb')">FTB</span>
                    <span class="label radius pointer" id="tekkit" onclick="select('tekkit')">Tekkit</span>
                    <span class="label radius pointer" id="pixelmon" onclick="select('pixelmon')">Pixelmon</span>
                    <span class="label radius pointer" id="prison" onclick="select('prison')">Prison</span>
                    <span class="label radius pointer" id="hub" onclick="select('hub')">Hub</span>
                    <span class="label radius pointer" id="roleplay" onclick="select('roleplay')">Roleplay</span>
                    <span class="label radius pointer" id="minigames" onclick="select('minigames')">Minigames</span>
                </div>
                <label for="" class="small-2 columns">Sorter efter
                    <select>
                        <option value="husker">Score</option>
                        <option value="starbuck">Oppetid</option>
                        <option value="hotdog">Spillere</option>
                        <option value="apollo">Status</option>
                    </select> </label>
            </div>
            <input type="checkbox" name="kategori" value="towny" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="factions" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="creative" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="survival" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="vanilla" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="pvp" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="pve" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="rpg" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="ftb" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="tekkit" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="pixelmon" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="prison" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="hub" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="roleplay" class="hidden" autocomplete="off"/>
            <input type="checkbox" name="kategori" value="minigames" class="hidden" autocomplete="off"/>
            </form>
    </div>
    </fieldset>
</div>
</div>
</div>
<div class="row">
    <div class="small-12 paddingzero columns">
        <table cellspacing="0" cellpadding="0" border="0" class="serverinfo noborder boxradius small-12">
            <thead>
                <tr>
                    <th class="rank"><i class="fa fa-star"></i></th>
                    <th class="name"><i class="fa fa-bookmark"></i> Navn</th>
                    <th class="banner show-for-medium-up"><i class="fa fa-tasks"></i> Banner</th>
                    <th class="players"><i class="fa fa-group"></i> Spillere</th>
                    <!--<th class="status"><i class="fa fa-power-off"></i> Status</th>-->
                    <th class="vote"><i class="fa fa-thumbs-up"></i> Point</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="rank text-center"><span class="serverrank label radius">1</span></td>
                    <td class="name"><a href="serverinfo.php">UPDenmark</a></td>
                    <td class="banner show-for-medium-up">
                    <div class="servername hide">
                        UPDenmark - fordi du lugter af gammel flødeost
                    </div><img src="img/server-banner.png" alt=""><span class="ip"><i class="fa fa-globe"></i>
 upd.primaservers.com</span></td>
                    <td class="players"><span class="label online radius">20/200</span></td>
                    <!--<td class="status"><span class="label online">Online</span></td>-->
                    <td class="vote">
                    <button class="button topradius tiny marginzero small-12" data-reveal-id="voteform">
                        Vote
                    </button>
                    <div class="status left small-12">
                        <span data-tooltip title="Understøtter Votifier" class="has-tip label success small-12">1405</span>
                    </div></td>
                </tr>
                <tr>
                    <td class="rank text-center"><span class="serverrank label radius">2</span></td>
                    <td class="name"><a href="serverinfo.php">WonderCraft Prison</a></td>
                    <td class="banner show-for-medium-up">
                    <div class="servername hide">
                        Wondercraft Prison
                    </div><img src="img/WorldsofMC banner.jpg" alt=""><span class="ip"><i class="fa fa-globe"></i>
 spil.wondercraft.dk:2556</span></td>
                    <td class="players"><span class="label offline radius alert">OFFLINE</td>
                    <!--<td class="status"><span class="label online">Online</span></td>-->
                    <td class="vote">
                    <button class="button tiny topradius marginzero small-12" href="">
                        Vote
                    </button>
                    <div class="status left small-12">
                        <span data-tooltip title="Understøtter ikke Votifier" class="has-tip label success small-12">1023</span>
                    </div></td>
                </tr>
                <tr>
                    <td class="rank text-center"><span class="serverrank label radius">3</span></td>
                    <td class="name"><a href="serverinfo.php">WonderCraft Prison</a></td>
                    <td class="banner show-for-medium-up"><img src="img/549227_0.png" alt=""><span class="ip"><i class="fa fa-globe"></i>
 spil.wondercraft.dk:2556</span></td>
                    <td class="players"><span class="label radius online">14/100</span></td>
                    <td class="vote">
                    <button class="button tiny topradius marginzero small-12" href="">
                        Vote
                    </button>
                    <div class="status left small-12">
                        <span data-tooltip title="Understøtter ikke Votifier" class="has-tip label success small-12">703</span>
                    </div></td>
                </tr>
                <tr>
                    <td class="rank text-center"><span class="serverrank label radius">4</span></td>
                    <td class="name"><span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="Serverens MOTD er her"><a href="serverinfo.php">Factions</a></span></td>
                    <td class="banner show-for-medium-up"><img src="img/549227_0.png" alt=""><span class="ip"><i class="fa fa-globe"></i> 
123.45.678.90:25565</span></td>
                    <td class="players"><span class="label radius online">14/100</span></td>
                    <td class="vote">
                    <button class="button tiny topradius marginzero small-12" href="">
                        Vote
                    </button>
                    <div class="status left small-12">
                        <span data-tooltip title="Understøtter Votifier" class="has-tip label success small-12">703</span>
                    </div></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div id="voteform" class="reveal-modal small" data-reveal>
    <div class="small-12">
        <h4>Stem på UPDenmark</h4>
        <h6>Votifier</h6>
        <form action="">
            <div class="row collapse">
                <div class="small-10 columns">
                    <input type="text" placeholder="Minecraft brugernavn">
                </div>
                <div class="small-2 columns">
                    <a href="" class="button postfix">Stem</a>
                </div>
            </div>
        </form>
    </div>
    <a class="close-reveal-modal">&#215;</a>
</div>
<div class="row">
    <div>
        <ul class="pagination">
            <li class="arrow unavailable">
                <a href="">&laquo;</a>
            </li>
            <li class="current custom">
                <a href="">1</a>
            </li>
            <li>
                <a href="">2</a>
            </li>
            <li>
                <a href="">3</a>
            </li>
            <li>
                <a href="">4</a>
            </li>
            <li class="unavailable">
                <a href="">&hellip;</a>
            </li>
            <li>
                <a href="">12</a>
            </li>
            <li>
                <a href="">13</a>
            </li>
            <li class="arrow">
                <a href="">&raquo;</a>
            </li>
        </ul>
    </div>
</div>
</body>
<?php
include 'footer.php';
?>