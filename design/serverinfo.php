<?php
include "header.php";
?>
<div class="server box">
    <div class="serverheader">
        <div class="server-pic">
            <a href=""><img src="img/VillagerFace.png" alt="" /></a>
        </div>
        <strong class="server-profile">UPDenmark</strong>
    </div>
    <div class="serverprofile-nav">
        <dl data-tab>
            <dd>
                <a href="#details" class="profile-icon"><i class="fa fa-info"></i> Detaljer</a>
            </dd>
            <dd>
                <a href="#plugins" class="servers-icon"><i class="fa fa-puzzle-piece"></i> Plugins</a>
            </dd>
            <dd>
                <a href="#players"><i class="fa fa-users"></i> Spillere</a>
            </dd>
            <dd>
                <a href="#addfav"><i class="fa fa-plus"></i> Favorit</i></a>
            </dd>
        </dl>
        <ul class="float-right">
            <a href=/edit/><i class="fa fa-gear"></a></i>
        </ul>
    </div>
    <div class="profile-biography">
        <div class="small-12 columns">
            <div class="tabs-content">
                <div class="content active" id="details">
                    <div class="small-12 large-8 columns">
                        <h3>Detaljer</h3>
                        <p>
                            Dette er serverens beskrivelse. Her kan du skrive om din servers koncept, folkene bag serveren eller fortælle folk, hvorfor de skal joine din server.
                        </p>
                    </div>
                    <div class="small-12 large-4 right columns paddingzero box">
                        <div class="serverprofile-details">
                            <h3>Info</h3>
                        </div>
                        <table class="serverdetails small-12">
                            <tbody>
                                <tr>
                                    <td class="detailsinfo">Ejer</td>
                                    <td><a href="?page=profile&id=2">Dennis</a></td>
                                </tr>
                                <tr>
                                    <td class="detailsinfo">IP</td>
                                    <td>37.187.135.28:25665</td>
                                </tr>
                                <tr>
                                    <td class="detailsinfo">Status</td>
                                    <td><span class="online">Online</span></td>
                                </tr>
                                <tr>
                                    <td class="detailsinfo">Spillere</td>
                                    <td>0/8</td>
                                </tr>
                                <tr>
                                    <td class="detailsinfo">Stemmer</td>
                                    <td>16</td>
                                </tr>
                                <tr>
                                    <td class="detailsinfo">Points</td>
                                    <td>1405</td>
                                </tr>
                                <tr>
                                    <td class="detailsinfo">Hjemmeside</td>
                                    <td><a href="https://primaservers.com">www.primaservers.com</a></td>
                                </tr>
                                <tr>
                                    <td class="detailsinfo">Version</td>
                                    <td><span class="label custom radius">1.7.5</span></td>
                                </tr>
                                <tr>
                                    <td class="detailsinfo">Tags</td>
                                    <td class="tags"><span class="label custom radius">Towny</span><span class="label custom radius">Hub</span><span class="label custom radius">Mini Games</span><span class="label custom radius">Economy</span><span class="label custom radius">Factions</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="content" id="plugins">
                    <h3>Brugerens tilføjede servere</h3>
                    <p>
                        Denne server bruger følgende plugins:
                    </p>
                    <ul>
                        <li>
                            WorldBorder 1.7.8
                        </li>
                        <li>
                            Votifier 1.9
                        </li>
                        <li>
                            Essentials Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsChat Pre2.13.1.6
                        </li>
                        <li>
                            SimpleVoteListener 2.6
                        </li>
                        <li>
                            EssentialsProtect Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsSpawn Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsAntiBuild Pre2.13.1.6
                        </li>
                        <li>
                            WorldBorder 1.7.8
                        </li>
                        <li>
                            Votifier 1.9
                        </li>
                        <li>
                            Essentials Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsChat Pre2.13.1.6
                        </li>
                        <li>
                            SimpleVoteListener 2.6
                        </li>
                        <li>
                            EssentialsProtect Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsSpawn Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsAntiBuild Pre2.13.1.6
                        </li>
                        <li>
                            WorldBorder 1.7.8
                        </li>
                        <li>
                            Votifier 1.9
                        </li>
                        <li>
                            Essentials Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsChat Pre2.13.1.6
                        </li>
                        <li>
                            SimpleVoteListener 2.6
                        </li>
                        <li>
                            EssentialsProtect Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsSpawn Pre2.13.1.6
                        </li>
                        <li>
                            EssentialsAntiBuild Pre2.13.1.6
                        </li>
                    </ul>
                </div>
                <div class="content" id="players">
                    <h3>Online spillere</h3>
                    <div class="playersonline">
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="inzorbia"><img src="../resources/images/face.php?u=inzorbia" alt="inzorbia avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="imHillKill"><img src="../resources/images/face.php?u=imHillKill" alt="imHillKill avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="oliepinden"><img src="../resources/images/face.php?u=oliepinden" alt="oliepinden avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="SuperAnden"><img src="../resources/images/face.php?u=SuperAnden" alt="SuperAnden avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="dvago"><img src="../resources/images/face.php?u=dvago" alt="dvago avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="Codda"><img src="../resources/images/face.php?u=Codda" alt="Codda avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="Notch"><img src="../resources/images/face.php?u=Notch" alt="Notch avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="AntVenom"><img src="../resources/images/face.php?u=AntVenom" alt="AntVenom avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="CaptainSparklez"><img src="../resources/images/face.php?u=CaptainSparklez" alt="CaptainSparklez avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="chrras"><img src="../resources/images/face.php?u=chrras" alt="chrras avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="the_last_dragon"><img src="../resources/images/face.php?u=the_last_dragon" alt="the_last_dragon avatar" /></span>
                        <span data-tooltip aria-haspopup="true" class="has-tip tip-bottom" title="Runner_Up"><img src="../resources/images/face.php?u=Runner_Up" alt="Runner_Up avatar" /></span>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include "footer.php";
?>