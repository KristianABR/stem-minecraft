</div>
<?php
import::js('vendor/modernizr.js');
import::js('vendor/jquery.js');
import::js('vendor/fastclick.js');
import::js('foundation.min.js');
import::js('script.js');
?>

<footer class="row">
		<hr />
		<p>
			&copy; Copyright Prima Servers 2014. All rights reserved. Design made by Prima Servers.
		</p>
</footer>
<script>
  $(document).foundation();
</script>
</div>
</html>