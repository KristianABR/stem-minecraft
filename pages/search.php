<?php
ob_start();

if (isset($_POST['search'])) {
	
	if (empty($_POST['search'])){
		header("Location: /search/Search%20For%20Everything/". $_POST['orderBy']);
	}else{
		header("Location: /search/".$_POST['search']."/". $_POST['orderBy']);
	}
}

echo server::search_form();

$q = (empty($_GET['q']) || $_GET['q'] === 'Search For Everything') ? FALSE : $_GET['q'];

include '3rdparty/Zebra_pagination.php';
$order = (isset($_GET['orderBy'])) ? $_GET['orderBy'] : FALSE;
$x = new server();
$result = $x -> find('all', 0, $order);

$servers = array();

if (is_bool($q)) {

	$servers = $result;

} else {

	foreach ($result as $server) {
		if (strpos(strtolower($server['name']), strtolower($_GET['q'])) !== false) {
			$servers[] = $server;
		}
		if (strpos(strtolower($server['description']), strtolower($_GET['q'])) !== false) {
			$servers[] = $server;
		}
	}

}

if (!empty($servers)) {
	$servers = array_map('unserialize', array_unique(array_map('serialize', $servers)));

	echo '<div class="row">';
	//print_r($x);

	// how many records should be displayed on a page?
	$records_per_page = 5;

	// instantiate the pagination object
	$pagination = new Zebra_Pagination();

	// set position of the next/previous page links
	$pagination -> navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

	// the number of total records is the number of records in the array
	$pagination -> records(count($servers));

	// records per page
	$pagination -> records_per_page($records_per_page);

	// here's the magick: we need to display *only* the records for the current page
	$servers = array_slice($servers, //  from the original array we extract
	(($pagination -> get_page() - 1) * $records_per_page), //  starting with these records
	$records_per_page //  this many records
	);

	//server::show($x,1);

	server::show($servers, 1);

	// render the pagination links
	echo '</div><div class="row">';
	$pagination -> render();
	echo '</div>';
} else {
	echo 'Ingen servere fundet!';
}
