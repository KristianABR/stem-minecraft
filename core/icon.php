<?php
	Error_Reporting( E_ALL | E_STRICT );
	Ini_Set( 'display_errors', true );
	require '../3rdparty/MinecraftServerPing.php';
if (isset($_GET['i'], $_GET['p'])) {
	$MQ_SERVER_ADDR = $_GET['i'];
	$MQ_SERVER_PORT = $_GET['p'];
	$MQ_TIMEOUT = 1;
	// Edit this <-

	$Info = false;
	$Query = null;

	try {
		$Query = new MinecraftPing($MQ_SERVER_ADDR, $MQ_SERVER_PORT, $MQ_TIMEOUT);

		$Info = $Query -> Query();

		if ($Info === false) {

			$Query -> Close();
			$Query -> Connect();

			$Info = $Query -> QueryOldPre17();
		}
	} catch( MinecraftPingException $e ) {
		$Exception = $e;
	}

	if ($Query !== null) {
		$Query -> Close();
	}

	if (isset($Info["favicon"])) {
		$icon = $Info["favicon"];
	} else {
		$icon = '../resources/images/no-servericon.png';
	}

	echo '<img src="'.$icon.'"/>';
}else{
	return 'Den nødvendige data er ikke tilgængelig.';
}
?>