<?php 
define('GD', 'Website by Game-Desk.net');
include 'config.php'; //including core.php

include VIEW_HEADER ;

import::css('foundation.css');
import::css('style.css');
import::css('font-awesome.min.css');

include VIEW_NAVIGATION ;
echo '<div data-alert class="alert-box ans">
        <a href="#" class="close">&times;</a>
      </div>';
/*--------------------------
    Vore dejlige ?page=$id
--------------------------*/
if(isset($_GET['page'])){
  $results = $results['0'] = '';

  $db->where('get_info', $_GET['page']);
  $results = $db->get('pages');

  if ($results) {
    $result = $results['0'];

    switch(htmlspecialchars($_GET['page'])) {
      
     case $result['get_info']:
      $pageName = $result['page_name'];
      require_once('pages/'.$result['page_file']);
      break;
    }
  }else{
           $pageName = 'Dashboard';
       require_once('site_welcome.php');
  }
}else{
  $pageName = 'Dashboard';
  require_once('site_welcome.php');
}

echo '<title>'.$pageName.' - '.CONFIG::site_name.'</title>';


include VIEW_FOOTER ;
?>