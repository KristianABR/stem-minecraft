<?php
ob_start();
include 'banner.classes.php';
include '../../3rdparty/MysqliDb.php';

$images = glob("backgrounds/*.jpg");



$db = new Mysqlidb("localhost", "stemminecraft", "ho2h9GTSTzuPM0yg", "stemminecraft");

if (isset($_GET['server'])) {

	$db -> where("name", htmlspecialchars_decode($_GET['server']));
	$result = $db -> get("servers");

	if (!empty($result)) {
		$ip = $result[0]['real_ip'];
		$port = $result[0]['real_port'];
	} else {
		$result[0]['online'] = 2;
	}
}

//$response = query($ip . ':' . $port, 'get');
if ($result[0]['online'] == 2) {
	// Set the content-type
	header('Content-Type: image/png');

	// Create the image
	$im = imagecreatefromjpeg("./". $images[ rand(0, max(array_keys($images))) ]);

	$error = "Ugyldig server data!";
	// Create some colors
	$red = imagecolorallocate($im, 255, 0, 0);

	// Replace path by your own font path
	$font = './OpenSans-Regular.ttf';

	// Add the text
	imagettftext($im, 20, 0, 100, 35, $red, $font, $error);

	// Using imagepng() results in clearer text compared with imagejpeg()
	imagepng($im);
	imagedestroy($im);
} elseif (!$result[0]['online']) {
	// Set the content-type
	header('Content-Type: image/png');

	// Create the image
	$im = imagecreatefromjpeg("./". $images[ rand(0, max(array_keys($images))) ]);

	$error = "Server Offline!";
	// Create some colors
	$red = imagecolorallocate($im, 255, 0, 0);

	// Replace path by your own font path
	$font = './OpenSans-Regular.ttf';

	// Add the text
	imagettftext($im, 20, 0, 145, 35, $red, $font, $error);

	// Using imagepng() results in clearer text compared with imagejpeg()
	imagepng($im);
	imagedestroy($im);
} else {

	// Set the content-type
	header('Content-Type: image/png');

	// Create the image
	$im = imagecreatefromjpeg("./". $images[ rand(0, max(array_keys($images))) ]);

	//Background Color

	// Create some colors
	$white = imagecolorallocate($im, 255, 255, 255);
	$grey = imagecolorallocate($im, 181, 181, 181);
	$black = imagecolorallocate($im, 0, 0, 0);
	$playercount = "" . $result[0]['players'] . "/" . $result[0]['maxPlayers'];
	$motd = substr($result[0]['name'], 0, 40);
	$ping = floatval($result[0]['ping']);
	$uptime = substr($result[0]['uptime'], 0, 5);

	// Replace path by your own font path
	$font = './OpenSans-Regular.ttf';

	// Add the text
	//Server Name
	imagettftext($im, 12, 0, 10, 25, $white, $font, $motd);

	//Online players
	imagettftext($im, 10, 0, 175, 45, $white, $font, "Players: " . $playercount . "");

	//Uptime
	imagettftext($im, 10, 0, 10, 45, $white, $font, "Uptime: ".$uptime."%");

	//Motd
	imagettftext($im, 12, 0, 30, 80, $white, $font, $motd);

	//Ping
	imagettftext($im, 10, 0, 350, 45, $white, $font, "Ping: " . ping($ip) . " ms");

	//By PrimaServers.com
	imagettftext($im, 5, 0, 375, 55, $white, $font, "Image by StemMinecraft.dk");

	// Using imagepng() results in clearer text compared with imagejpeg()
	imagepng($im);
	imagedestroy($im);

}
$output = ob_get_clean();
echo $output;
?>