<?php

if ( $loggedIn ) {
	
	if ( !empty($_POST) ) {

			if ($isAdmin && isset($_POST['id'])){
				$db->where('id', $_POST['id']);
				$result = $db->get('users'); 
			}else{

				$db->where('id', $_SESSION['id']);
				$result = $db->get('users'); 
			}




				//Email
				if ( !empty($_POST['email']) ) {
					if ( filter_var($_POST['email'],  FILTER_VALIDATE_EMAIL) !== false ){

						$data['email'] = (string) $_POST['email'];

					} else {
						echo 'Ikke et godkendt email format.';
					}
				}


				//New Password
				if ( !empty($_POST['newPass']) ) {
					if ( !empty($_POST['newPass2']) ){

						if ( $_POST['newPass'] === $_POST['newPass2'] ) {

							$data['password'] = (string)  hash('sha512', $_POST['newPass'] . $result[0]['token']);

						} else {
							echo 'Dine to passwords passer ikke overens.';
						}

					} else {
						echo 'Du har ikke gengivet dit nye password.';
					}
				}

				//Coverbillede
				if ( !empty($_POST['cover']) ) {

					if ( filter_var($_POST['cover'], FILTER_VALIDATE_URL) !== false) {

						$data['cover'] = (string) $_POST['cover'];

					}else{
						echo 'Det ligner ikke et billede, som du linker til, til dit coverbillede.';
					}

				}

				//Biografi
				if ( !empty($_POST['bio']) ) {

					$data['bio'] = (string) $_POST['bio'];

				}

				//Show_favorite
				if ( !empty($_POST['show_favorite']) ) {

					$data['show_favorite'] = "1";
					
				}

				//Show_gallery
				if ( !empty($_POST['show_gallery']) ) {

					$data['show_gallery'] = "1";
					
				}


				if ( !empty($data) ) {

					if ($isAdmin && isset($_POST['id'])){
						$db->where('id', $_POST['id']);
						$db->update('users', $data); 

						header("Location: /profile/".$_POST['id']);
					}else{

						$db->where('id', $_SESSION['id']);
						$db->update('users', $data); 

						header("Location: /profile/".$_SESSION['id']);

					}

				}


	} else { 

		if ($isAdmin && isset($_GET['id'])){

			$db->where("id", $_GET['id']);
			$result = $db->get("users");

		}else{

			$db->where("id", $_SESSION['id']);
			$result = $db->get("users");

		}




?>

<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script>
	tinymce.init({
		selector : 'textarea#MCE'
	}); 
</script>

<div class="row">
	<form method="POST" action="/profile/edit/">
		<fieldset id="" class="">
			<legend>
				Adgangskode og email
			</legend>
			<div class="small-12 columns">
				<div class="editProfileField">
					<div class="small-2 columns">
						Nuværende adgangskode
					</div>
					<div class="small-6 left columns">
						<input type="password" name="password" requried />
					</div>
				</div>

				<div class="editProfileField">
					<div class="small-2 columns">
						Ny adgangskode
					</div>
					<div class="small-6 left columns">
						<input type="password" name="newPass" />
					</div>
				</div>

				<div class="editProfileField">
					<div class="small-2 columns">
						Gentag adgangskode
					</div>
					<div class="small-6 left columns">
						<input type="password" name="newPass2" />
					</div>
				</div>

				<div class="editProfileField">
					<div class="small-2 columns">
						Email
					</div>
					<div class="small-6 left columns">
						<input type="text" name="email" <?php echo 'value="' . $result[0]['email'] . '"'; ?> />
					</div>
				</div>

				<input type="submit" value="Go">
		</fieldset>
	</form>
	<form method="POST" action="/profile/edit/">
		<fieldset id="" class="">
			<legend>
				Profil
			</legend>
			<div class="small-12">
				<div class="editProfileField">
					<div class="small-2 columns">
						Cover
					</div>
					<div class="small-10 columns">
						<input name="cover" type="text"
						<?php 	echo ( isset( $result[0]['cover'] ) ) ? 'value=" ' . $result[0]['cover']. ' "' : '' ;?>
						/>
					</div>
				</div>

				<div class="editProfileField">
					<div class="small-2 columns">
						Profiltekst
					</div>
					<div class="small-10 columns">
						<textarea id="MCE" name="bio" rows="8" cols="40">
					<?php 	echo ( isset( $result[0]['bio'] ) ) ? $result[0]['bio'] : '' ;?> 
					</textarea>												
					


					</div>
				</div>

				<div class="editProfileField">
					<div class="small-2 columns">
						Vis favoritter
					</div>
					<div class="small-10 columns">
						<input type="checkbox" name="show_favorite"
						<?php echo ($result[0]['show_favorite']) ? 'checked' : ''; ?>
						/>
					</div>
				</div>

				<div class="editProfileField">
					<div class="small-2 columns">
						Vis billeder
					</div>
					<div class="small-10 columns">
						<input type="checkbox" name="show_gallery"
						<?php echo ($result[0]['show_gallery']) ? 'checked' : ''; ?>
						/>
					</div>
				</div>

				<input type="submit" value="Go">
			</div>
		</fieldset>
	</form>
</div>
</div>

<?php
}

} else {

echo 'Du er ikke logged ind.';

}
?>