<?php
require_once DIR_CORE . 'user.php';

if ($loggedIn) {
	$db -> where('id', $_SESSION['id']);
	$results = $db -> get('users');

	if ($_POST['newPassword'] == $_POST['confNewPassword']) {
		$newPasswd = hash('sha512', $_POST['newPassword'] . $results[0]['token']);

		$oldPasswd = hash('sha512',$_POST['oldPassword'] . $results[0]['token']);

		if ($oldPasswd == $results[0]['password']) {

			$data = array('password' => $newPasswd);
			$db -> where('id', $_SESSION['id']);
			if ($db -> update('users', $data))
				echo 'Ændring udført';
		}

	} else {
		echo 'Dine to passwords stemmer ikke overens.';
	}

} else {
	echo '<br>Der skete en fejl, prøv igen!';
}

echo '<br>';
?>