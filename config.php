<?php
class CONFIG {
	const 	db_host = 'localhost',
			db_user = 'stemminecraft',
			db_pass = 'ho2h9GTSTzuPM0yg',
			db_name = 'stemminecraft',
			
			site_name = 'Stem Minecraft',
			site_nameshort = 'StemMC',
			
			domain = 'StemMinecraft.dk',
			
			hash_algo = 'sha512',
			
			debug = '1';
}


//if (!defined('GD')) die('This file cannot be accessed directly');
define('DIR_BASE', dirname( __FILE__ ) . '/');

define('VIEW_HEADER',   	DIR_BASE . 'header.php');
define('VIEW_NAVIGATION',   DIR_BASE . 'navigation.php');
define('VIEW_FOOTER',   	DIR_BASE . 'footer.php');

define('DOMAIN', 'http://'.CONFIG::domain.'/');

define('DIR_RESOURCES', DOMAIN . '/resources/');
define('DIR_STYLES', 	DIR_RESOURCES . 'css/');
define('DIR_JS', 		DIR_RESOURCES . 'js/');
define('DIR_ERROR', 	DIR_RESOURCES . 'error/');
define('DIR_FONT', 		DIR_RESOURCES . 'fonts/');
define('DIR_IMG', 		DIR_RESOURCES . 'images/');
define('DIR_LANG', 		DIR_RESOURCES . 'lang/');

define('DIR_CORE', DIR_BASE . 'core/');
define('DIR_3RDPARTY', DIR_BASE . '3rdparty/');
define('DIR_PAGES', DIR_BASE . 'pages/');





include('core/core.php');
?>
