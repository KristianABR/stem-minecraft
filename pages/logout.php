<?php
if (!defined('GD')) die('This file cannot be accessed directly');
    if ($loggedIn) {
    	require_once DIR_CORE.'user.php';
    	user::logout();
  		echo 'Du er nu logget ud.';
  		header("Location: ".DOMAIN);
    } else {
    	echo 'Du er ikke logget ind.';
    }
?>