<?php
require DIR_CORE.'user.php';

if (isset($_GET['t'])) {
	$db->where('token', $_GET['t']);
	$results = $db->get('recovery');
		if (isset($results[0])) {
			$db->where('id',$results[0]['userId']);
				$results = $db->get('users');

			$newPass = user::genRandString(8);
			$encPass = hash("sha512", $newPass . $results[0]['token']);
			
			$data = array (
    			'password' => $encPass
			);

			$db->where('id', $results[0]['id']);
				$db->update('users', $data);
				 echo 'Dit nye password er: '.$newPass;


			$db->where('token', $_GET['t']);
				$db->delete('recovery');

		}else{
			echo 'Enten er dit password allerede ændret, ellers har du tastet forkert.';
		}
}else{
	if (isset($_POST['email'])) {
		$db->where('email',$_POST['email']);
		$results = $db->get('recovery');

		if (!isset($results[0])) {
			if (!$loggedIn) {

				$db->where('email',$_POST['email']);
				$results = $db->get('users');

					if (!empty($results[0])) {
						$userId = $results[0]['id'];
						$token = user::genRandString(32);
						$email = $_POST['email'];

						$data = array(
    						'userId' => $userId,
    						'token' => $token,
    						'email' => $email,
						);

						$db->insert('recovery', $data);

								require_once (DIR_3RDPARTY . 'mailKit.php');

								sendMail($email, 
									'noreply@' . CONFIG::domain, 
										CONFIG::site_name . ' | Gendanelse af bruger', 

									'Hej ' . $results[0]['username'] . ',
				 							<br> Vi har registreret, at du gerne vil gendanne dit password.
				 							<br>Følg linket og få dit nye password.
				 							<br><a href="http://'.CONFIG::domain.'/index.php?page=recoveryDo&t='.$token.'">http://'.CONFIG::domain.'/index.php?page=recoveryDo&t='.$token.'</a>
											<br><br>Husk, at du altid kan ændre dit password i menuen, der ligger under \'Profil\'.
											<br><br> God fornøjelse. <br>Hilsner <a href="http://' . CONFIG::domain . '/">' . CONFIG::domain . '</a>.');
								echo 'Vi har sendt dig en mail, med oplysninger. Tjek den og sørg for, at ændre koden igen bagefter.';
					}else{
						echo 'Denne email findes ikke i vores system.';
					}
			}else{
				echo 'Du kan ikke gendanne en bruger, hvis du er logget ind.';
			}
		}else{
			echo 'Du har allerede modtaget en mail med oplysninger. Send email igen!';
		}
	}else{
		echo 'Du skal udfylde formularen, hvis du vil gendanne en bruger.';
	}
}
?>