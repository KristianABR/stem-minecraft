<?php
include '3rdparty/Zebra_pagination.php';

$db->orderBy("id","Desc");
$result = $db->get('news');

	echo '</div>
<div class="header">
	<div class="row">
		<h2>'.$result[0]['title'];

            if ( $isAdmin ) {
                echo '<small><a href="index.php?page=news">Ny besked</a></small>';
            }

		echo '</h2><p>
			'.$result[0]['body'].'
		</p>
        Tilføjet: '.$result[0]['time'].'
	</div>
</div>';
echo server::search_form();

	$orderby = (isset($_GET['orderBy'])) ? $_GET['orderBy'] : FALSE;
	
	$servers = new server();
	$x = $servers -> find('all', 0, $orderby);


	//print_r($x);

        // how many records should be displayed on a page?
        $records_per_page = 5;

        // instantiate the pagination object
        $pagination = new Zebra_Pagination();

        // set position of the next/previous page links
        $pagination->navigation_position(isset($_GET['navigation_position']) && in_array($_GET['navigation_position'], array('left', 'right')) ? $_GET['navigation_position'] : 'outside');

        // the number of total records is the number of records in the array
        $pagination->records(count($x));

        // records per page
        $pagination->records_per_page($records_per_page);

        // here's the magick: we need to display *only* the records for the current page
        $x = array_slice(
            $x,                                             //  from the original array we extract
            (($pagination->get_page() - 1) * $records_per_page),    //  starting with these records
            $records_per_page                                       //  this many records
        );

		server::show($x,1);
	
        // render the pagination links
       echo '</div><div class="row">';
        $pagination->render();
	echo '</div>';
?>