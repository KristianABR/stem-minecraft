<?php

// Enter your public key (key.pub) here
$open_SSL_pub="-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvpWIOCx4CANki0xCQcSq
P/8uKTNhKqyushCaABpkE4c8Mx9xJrzmAMy8dzd3SCY/tpX9NUXwZXJBHP35uri7
Jk3ZHY2sI4c2E+DEcCTyc2Y7H1DdeyO37OApKbOXBclq2UqOAPyV+NNvfrtIRwOz
nRI0r9exKKky+tyzt/cUPOWcgxM62bCg1skxiD/XgcHoSIsYgyOWv9KT9xqCXJxu
Xucpk7HQZUMcDIjv/6zJNYA+idn9lIz/Ta+2+mtntbZaNX0CQQBAhQGH9EyGHA4s
utgdrk/mCBGZSxVTrlrkyvqLZibHrcJKrnXhCsry5hZMRvuhYbKHtWYBhFv4WtaD
YwIDAQAB
-----END PUBLIC KEY-----";
 
 // Enter your private key (key.priv) here
$open_SSL_priv="-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAvpWIOCx4CANki0xCQcSqP/8uKTNhKqyushCaABpkE4c8Mx9x
JrzmAMy8dzd3SCY/tpX9NUXwZXJBHP35uri7Jk3ZHY2sI4c2E+DEcCTyc2Y7H1Dd
eyO37OApKbOXBclq2UqOAPyV+NNvfrtIRwOznRI0r9exKKky+tyzt/cUPOWcgxM6
2bCg1skxiD/XgcHoSIsYgyOWv9KT9xqCXJxuXucpk7HQZUMcDIjv/6zJNYA+idn9
lIz/Ta+2+mtntbZaNX0CQQBAhQGH9EyGHA4sutgdrk/mCBGZSxVTrlrkyvqLZibH
rcJKrnXhCsry5hZMRvuhYbKHtWYBhFv4WtaDYwIDAQABAoIBAAfdwiaXZ0jlYvQA
7McsJ97GWJBox3zsbbV5y/FvJWxM66YsiQ4RQ+kKikjdaDX5Ji4SGMZnIZn+UkOx
E+qqD0dwdSKsI0vAG/JMF6+NRtiOgqgzPUEE42WKhwhqmjQp8RgQ5LWTt1jy2SpA
7qXHuFUmWDhdzbkg6rLCA0T3lMVCnqRXG/5Qlswk6hEa0Vo/D8h/U9jIUfBATqHS
7XQJpDZ3uktdawzWYMM6q6OxhclpYdNO3FVXN2L6buU7DLbqWHNFxjQRFJLi2VM4
fWjrC0mplYFKqcgAQ8sz5v0AWJw79mO3ZRAP+lTs+DAiwx5/2IZJQTlhWEpuhVXj
hvVJAIkCgYEA6iFdh+7OCLouY9dHLbiqGi9WSXwdsBjLfGt5FXoZRqdMOUG7JSLu
EsDCkeH/AjXi+ZQx4lhvGAuR/ymh7s78LBdEDm31anjUtAvjmKnnyDzG5cCWLncV
i1TLrrsYefzhrpGUhX5rG+Wg/IWQkDFZY0F6PTFJc+/0oE8zmKI3qL0CgYEA0GLe
dPutBH/QcbMosmRDPejVwg0oqPzv6pRI0QRKKI8Cr6wuqARluxD2dPv4x/N8B0iv
bXUh523bSwMLuaNWlbCbzkR9AWVJGuGEyVmAL53g14CvTMAEHkpbOq3bZDhSuRZF
htmX4P/6W/IReibgZdauA1APlFAbkEykLUvJ7p8CgYBwUbFdFOMirK60lwI+nOh9
x1fNyQ00/bAg8C4Wb4zQqFGY5j+xgSBVPsQY2CJOHSvWL5H+1A33qxVPI9nbhtDT
FhZAFeA2Q+FGvq1g/6GCHiYM8mcZw2dCgFt2Z6tGWlqqNIzbvdoEeBTTEABaLFKF
m1PZJ1vlkqf4gAZwouGwwQKBgQCYFJ/PdmGl1/XzFNKNprigNQ3TyjCza61YYJHn
4R6SDRTjymBehJ8N7+o+JCpDGwKYekZ1Nrfgo38YdoeXGoq1QifjWH52h0ncEVwM
aoIvviP6gtLsy3H1/UQpRfrIYIaWx5dEPAco0QgRFFAG+tpn2E404rgMAI6tOBHV
cOtVdwKBgBFg2W46O4xQrZIvxSa1PYViyoHOSaApQrO+DY8yEXTJ+co8iCktBAkZ
eESKHoXVLp6/N7114yW08s62mqSNoVEjxbLuWxhf2DkfDTWqQGn4/WQ8FQlBO83r
D2B5yA8Kluzdq4dVKABqx7Ehw2v9Q/X7W/q81VAZBFZWy8n4kEKk
-----END RSA PRIVATE KEY-----";

// DKIM Configuration

// Enter signing domain
// This should be the root domain (not subdomain!)
$DKIM_d='primaservers.com';  

// Enter sender name
$senderName = "Kristian á Brúnni";

// Selector, defines where the public key is stored in the DNS
//    $DKIM_s._domainkey.$DKIM_d
// Mandatory
$DKIM_s='vote';

?>