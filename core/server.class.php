<?php
/**
 * Server for voting
 */
class server {
	public $_name;
	public $_ip;
	public $_port;
	public $_desc;
	public $_user;
	public $_website;
	public $_id;
	public $_votes;

	function __construct($name = '', $ip = '', $port = '', $desc = '', $user = '', $website = '') {
		global $db;
		if ($db) {
			$this -> _db = $db;

			if (isset($name, $ip, $port, $desc, $user, $website)) {
				$this -> _name = $name;
				$this -> _ip = $ip;
				$this -> _port = $port;
				$this -> _desc = $desc;
				$this -> _website = $website;
				$this -> _votes = '0';

				$this -> _user = $user;

				//Counting rows in our table users for setting a ID
				$results = $db -> query('SELECT COUNT(*) AS count FROM servers');
				$id = $results[0]['count'] + 1;

				$this -> _id = $id;
			} else {
				return 'Ingen server init. Kun enkelte funktioner er tilgængelige.';
			}

		} else {
			return 'Du er ikke connected til databasen.';
		}
	}

	public static function ping($ip, $port = "80") {

		if (isset($ip)) {
			$starttime = microtime(true);
			$file = fsockopen($ip, $port, $errno, $errstr, 1);
			$stoptime = microtime(true);
			$status = 0;

			if (!$file) {
				$ping = NULL;

			} else {
				fclose($file);
				$status = ($stoptime - $starttime) * 1000;

				$ping = explode('.', $status);
				$ping = $ping[0];

			}
		} else {
			$ping = "No IP set.";
		}
		return $ping;
	}

	public static function vote($id = '') {
		global $db;
		global $_SESSION;
		if (!empty($id)) {
			if (is_int($id)) {
				$x = self::find($id);

				if (is_array($x[0])) {
					if (!in_array("votes", $x[0])) {
						echo 'Id\'et er ikke til at finde.';
					} else {
						$comb = $id . '_' . $_SESSION['id'];
						$db -> where('serverId_userId', $comb);

						$result = $db -> get('votes');

						if ($result) {
							echo 'Du har allerede stemt på denne server engang i dag.';
						} else {

							$db -> where('id', $_SESSION['id']);
							$res = $db -> get('users');

							$data = array("timesVoted" => $res[0]['timesVoted'] + 1);
							$db -> where('id', $_SESSION['id']);
							$db -> update('users', $data);

							$data = array('serverId_userId' => $comb);

							$vote = $db -> insert('votes', $data);
							$votes = $x[0]['votes'] + 1;
							$data = array('votes' => $votes);
							$db -> where('id', $id);
							if ($db -> update('servers', $data))
								echo 'Tak for din stemme.';
							if ($x[0]['votifier']) {
								require 'core/votifier.php';

								$PUB_KEY = $x[0]['votifierPublicKey'];

								$votifier = new MinecraftVotifier($PUB_KEY, $x[0]['ip'], $x[0]['votifierPort'], 'Prima Servers Voting System');

								$db -> where('id', $_SESSION['id']);
								$results = $db -> get('users');

								$votifier -> sendVote($results[0]['minecraftaccount']);
							}
						}

					}
				} else {
					echo 'Id\'et er ikke til at finde.';
				}

			} else {
				echo 'Du det skal være id\'et du stemmer på.';
			}
		} else {
			echo 'Du skal have et id at stemme på.';
		}
	}

	public static function show($servers, $state) {
		global $db;

		if (is_array($servers) && $state === 1) {
			echo '
			<div class="small-12 paddingzero columns">
			  <table cellspacing="0" cellpadding="0" border="0" class="serverinfo noborder small-12">
				<thead>
				  <tr>
					<th class="rank"><i class="fa fa-star"></i></th>
		            <th class="name"><i class="fa fa-bookmark"></i> Navn</th>
		            <th class="banner show-for-large-up"><i class="fa fa-tasks"></i> Banner & Info</th>
		            <th class="players"><i class="fa fa-group"></i> Spillere</th>
		            <th class="vote"><i class="fa fa-thumbs-up"></i> Vote</th>
				  </tr>
				</thead>
				<tbody>';
			foreach ($servers as $server) {
				$even = false;
				if ($server['votifier']) {
					$votifier = '<span data-tooltip title="Understøtter Votifier" class="has-tip label success small-12">Votifier</span>';
				} else {
					$votifier = '<span data-tooltip title="Understøtter ikke Votifier" class="has-tip label alert small-12">Votifier</span>';
				};

				if ($server['id'] % 2 == 0) {
					$even = 'class="even"';
				}

				echo '<tr ' . $even . '>
        				<td class="rank">
				       		<span class="serverrank">' . $server["rank"] . '</span>
				        </td>
			        <td class="name"><a href="/server/' . $server["id"] . '">' . $server["name"] . '</a></td>
			       		<td class="banner show-for-large-up">
     			  			<img src="/banner/' . htmlspecialchars($server['name']) . '.png" alt="Minecraft status banner for ' . $server['name'] . '">
       						<span class="ip">' . $server['ip'];
				if ($server['port'] != "25565") {echo ':' . $server['port'];
				}
				echo '</span>
        				</td>
        			<td class="players">';
				if ($server['online']) { echo '<span class="label online radius">' . $server['players'] . '/' . $server['maxPlayers'] . ' </span>';
				} else {echo '<span class="label offline radius alert">OFFLINE</span>';
				}
				echo '</td>
        			<td class="vote">
        				<button class="button tiny marginzero small-12" onclick="vote(' . $server['id'] . ')">Vote</button>
        				<div class="status left small-12">
        					' . $votifier . '
        				</div>
        			</td>
      				</tr>';
			}
			echo '</tbody>
      				</table>
				</tbody>
      		</table>
	  		</div>';
		}
	}

	public static function servShow($id) {
		global $db;
		global $loggedIn;
		global $isAdmin;
		if (isset($id)) {

			$db -> where('id', $id);
			$server = $db -> get('servers');

			if (!is_null($server[0]["icon"])) {
				$icon = $server[0]["icon"];
			} else {
				$icon = DIR_IMG . 'no-servericon.png';
			}

			echo '<div class="server box">
					<div class="serverheader">
						<div class="server-pic">
							<a href=""><img src="' . $icon . '" alt="' . $server[0]["name"] . '" /></a>
						</div>
						<strong class="server-profile">' . $server[0]["name"] . '</strong>
					</div>
					<div class="serverprofile-nav">
						<dl data-tab>
							<dd>
								<a href="#details" class="profile-icon"><i class="fa fa-info"></i> Detaljer</a>
							</dd>';
			if ($server[0]["showPlugins"]) {echo '<dd>
								<a href="#plugins" class="servers-icon"><i class="fa fa-puzzle-piece"></i> Plugins</a>
							</dd>';
			}
			echo '<dd>
								<a href="#playersonline"><i class="fa fa-users"></i> Spillere</a>
							</dd>';
			if ($loggedIn) {

				$db -> where('id', $_SESSION['id']);
				$result = $db -> get('users');

				if (strpos($result[0]['favorite'], $_GET['id']) !== FALSE) {
					echo '<a href="/favorite/del/' . $server[0]['id'] . '"><i class="fa fa-minus"></i> Favorit</i></a>';
				} else {
					echo '<a href="/favorite/add/' . $server[0]['id'] . '"><i class="fa fa-plus"></i> Favorit</i></a>';
				}
			}
			echo '</dl>';
			if ($isAdmin || $loggedIn && $_SESSION['username'] === $server[0]['username']) {
				echo '<a href=/server/edit/' . $server[0]['id'] . ' class="float-right"><i class="fa fa-gear"></a></i>';
			}
			echo '</div>
					<div class="profile-biography">
					<div class="small-12 large-7 columns">
						<div class="tabs-content">
							<div class="content active" id="details">
								<h3>Detaljer</h3>
								<p>' . $server[0]["description"] . '</p>
							</div>';
			if ($server[0]["showPlugins"]) {
				echo '<div class="content" id="plugins">';
				if (!empty($server[0]['plugins'])) {
					echo '<h3>Plugins</h3>
										<p>Denne server bruger følgende plugins:</p>
										<ul>';
					$x = explode(',', $server[0]['plugins']);
					foreach ($x as $y) {
						echo '<li>' . $y . '</li>';
					}
					echo '</ul>';
				} else {
					echo 'Kan ikke finde nogle plugins på denne server.';
				}
				echo '</div>';
			} else {
				echo 'Kan ikke finde nogle plugins på denne server.';
			}

			echo '<div class="content" id="playersonline">';

			if ($server[0]['whichPlayers']) {
				$players = explode(',', $server[0]['whichPlayers']);
				foreach ($players as $player) {
					$id = user::find($player, 'minecraftaccount', 'id');
					if ($id) {
						echo ' <p><a href="/profile/' . $id . '/"><img width="15px" src="' . DIR_IMG . 'face.php?u=' . $player . '"" /> ' . $player . '</a></p>';
					} else {
						echo ' <p><img width="15px" src=' . DIR_IMG . 'face.php?u=' . $player . '> ' . $player . '</p>';
					}
				}
			} else {
				echo 'Der er ingen spillere online, i øjeblikket.';
			}
			echo '
									</div>
								</div>
						</div>
					<div class="small-12 large-5 right columns paddingzero box">
						<div class="serverprofile-details">
							<h3>Info</h3>
						</div>
						<table class="serverdetails small-12">
							<tbody>
								<tr>
									<td class="detailsinfo">Ejer</td>';
			$db -> where('username', $server[0]['username']);
			$user = $db -> get('users');

			if (isset($user[0])) {
				echo '<td><a href="/profile/' . $user[0]['id'] . '">' . $server[0]['username'] . ' (' . $user[0]['minecraftaccount'] . ')</a></td>';
			} else {
				echo '<td><a href="/profile/0">Brugeren eksistere ikke</a></td>';
			}

			$port = ($server[0]['port'] != '25565') ? ':' . $server[0]['port'] : '';

			echo '</tr>
								<tr>
									<td class="detailsinfo">IP</td>
									<td>' . $server[0]['ip'] . $port . '</td>
								</tr>
								<tr>
									<td class="detailsinfo">Status</td>';
			if ($server[0]['online']) {
				echo '<td><span class="online">Online</span></td>';
			} else {
				echo '<td><span class="offline">Offline</span></td>';
			}
			echo '
								</tr>
								<tr>
									<td class="detailsinfo">Spillere</td>
									<td>' . $server[0]['players'] . '/' . $server[0]['maxPlayers'] . '</td>
								</tr>
								<tr>
									<td class="detailsinfo">Stemmer</td>
									<td>' . $server[0]['votes'] . '</td>
								</tr>';
			if (!empty($server[0]['website'])) {
				echo '
									<tr>
										<td class="detailsinfo">Hjemmeside</td>
										<td><a href="' . $server[0]['website'] . '">' . $server[0]['website'] . '</a></td>
									</tr>';
			}
			if (!empty($server[0]['version'])) {
				echo '
									<tr>
										<td class="detailsinfo">Version</td>
										<td><span class="label custom radius">' . $server[0]['version'] . '</span></td>
									</tr>';
			}
			if (!empty($server[0]['categories'])) {
				$categories = explode(',', $server[0]['categories']);

				echo '<tr>
									<td class="detailsinfo">Tags</td>
									<td class="tags">';
				foreach ($categories as $tag) {
					echo '<span class="label custom radius">' . $tag . '</span>';
				}

				echo '</td></tr>';
			}
			echo '
							</tbody>
						</table>
					</div>
				</div>
			</div>';

		} else {
			return 'Svaret var ikke en array. server::servShow()';
		}
	}

	public static function find($arg = '', $show = '', $orderBy = '') {
		global $db;
		if (!empty($arg)) {
			if (is_int($arg)) {
				$db -> where('id', $arg);
				$result = $db -> get('servers');

				if ($result) {
					if (!empty($show)) {
						self::show($result, $show);
					} else {
						return $result;
					}

				} else {
					echo 'Det ser ikke ud til, at vi har nogen server i vores database, med id\'et <i>' . $arg . '</i>.';
				}
			} elseif ($arg === 'all') {
				if (isset($orderBy)) {

					switch ($orderBy) {
						case 'rank-desc' :
							$db -> orderBy("rank", "desc");
							break;

						case 'votes-asc' :
							$db -> orderBy("votes", "asc");
							break;

						case 'votes-desc' :
							$db -> orderBy("votes", "desc");
							break;

						case 'dateSubmitted-asc' :
							$db -> orderBy("dateSubmitted", "asc");
							break;

						case 'dateSubmitted-desc' :
							$db -> orderBy("dateSubmitted", "desc");
							break;

						case 'score-asc' :
							$db -> orderBy("score", "asc");
							break;

						case 'score-desc' :
							$db -> orderBy("score", "desc");
							break;

						case 'players-asc' :
							$db -> orderBy("players", "asc");
							break;

						case 'players-desc' :
							$db -> orderBy("players", "desc");
							break;

						default :
							$db -> orderBy("rank", "asc");
							break;
					}

				} else {
					$db -> orderBy("rank", "asc");
				}

				$x = $db -> get('servers');

				if ($x) {
					if (!empty($show)) {
						self::show($x, 1);
					} else {
						return $x;
					}
				} else {
					return 'Der skete en fejl.';
				}

			} elseif (is_array($arg)) {

				foreach ($arg as $server) {

					$db -> where('id', (int)$server);
					$result = $db -> get('servers');

					if ($result) {
						if (!empty($show)) {
							self::show($result, $show);
						} else {
							return $result;
						}

					} else {
						echo 'Det ser ikke ud til, at vi har nogen server i vores database, med id\'et <i>' . $arg . '</i>.';
					}

				}

			} else {
				$db -> where('username', $arg);
				$result = $db -> get('users');

				if ($result) {
					$x = $result[0]['username'];
					if ($x) {
						$db -> where('username', $x);
						$db -> orderBy("rank", "asc");
						$db -> orderBy("votes", "desc");
						$db -> orderBy("dateSubmitted", "Desc");
						$results = $db -> get('servers');
						if ($results) {
							if (!empty($show)) {
								self::show($results, 1);
							} else {
								return $result;
							}
						}
					}
				} else {
					return 'Det ser ikke ud til, at brugeren <i>' . $arg . '</i> findes.';
				}
			}
		} else {
			return 'Der skete en fejl, prøv igen.';
		}
	}

	public static function dnslookup($host) {
		$ips = array();
		if (preg_match_all('/Address: ((?:\d{1,3}\.){3}\d{1,3})/', `nslookup $host`, $match) > 0) {
			$ips['real_ip'] = $match[1];
		}

		if (empty($ips)) {
			if (preg_match_all("/service = (\d{1,3}) (\d{1,3}) (\d{4,7}) (\w.{1,50})/", `nslookup -type=SRV _minecraft._tcp.$host`, $match) > 0) {
				$ips['port'] = $match[3];
				$ips['real_ip'] = $match[4];
			} else {
				$ips = NULL;
			}
		}

		return $ips;
	}

	public static function search_form() {
		global $_GET;
		$value = (isset($_GET['q'])) ? 'value="' . $_GET['q'] . '"' : '';

		return '<div class="row">
	<div class="serversearch small-12">
		<fieldset>
			<legend>
				Find server
			</legend>
			<form method="POST" action="/search">
			<div class="row collapse">
				<div class="small-2 columns">
					<span class="prefix">Navn</span>
				</div>
				<div class="small-8 columns">
					<input type="text" name="search" placeholder="Søg efter server..." ' . $value . '>
				</div>
				<div class="small-2 columns">
					<input type="submit" class="button rightradius postfix" value="Go">
				</div>
              
				<div class="small-9 columns">
					<label>Søg i følgende tag</br></label>
					<div class="tag">
						<input type="checkbox">
						SMP
					</div>
					<div class="tag">
						<input type="checkbox">
						Survival
					</div>
					<div class="tag">
						<input type="checkbox">
						Towny
					</div>
					<div class="tag">
						<input type="checkbox">
						Factions
					</div>
					<div class="tag">
						<input type="checkbox">
						Roleplay
					</div>
					<div class="tag">
						<input type="checkbox">
						Tekkit
					</div>
					<div class="tag">
						<input type="checkbox">
						Feed The Beast
					</div>
					<div class="tag">
						<input type="checkbox">
						Minigames
					</div>
					<div class="tag">
						<input type="checkbox">
						Hub
					</div>
					<div class="tag">
						<input type="checkbox">
						Economy
					</div>
				</div>
				</form>
				<label for="" class="small-1 columns">Sorter efter
					<select name="orderBy">
						<option value="rank-asc">			Rank-ASC</option>
						<option value="rank-desc">			RANK-DESC</option>
						<option value="votes-asc">			VOTES-ASC</option>
						<option name="orderBy" value="votes-desc">			VOTES-DESC</option>
						<option value="dateSubmitted-asc">	DATESUBMITTED-ASC</option>
						<option value="dateSubmitted-desc">	DATESUBMITTED-DESC</option>
						<option value="score-asc">			SCORE-ASC</option>
						<option value="score-desc">			SCORE-DESC</option>
						<option value="players-asc">		PLAYERS-ASC</option>
						<option value="players-desc">		PLAYERS-ASC</option>
					</select> </label>
			</div>
		</fieldset>
	</div>
</div>
</div><div class="row">';
	}

	public static function query($server, $method = "query") {
		if (isset($server)) {
			if (isset($method)) {
				switch ($method) {
					case 'status' :
						$res = get("http://minecraft-api.com/v1/status/?server=" . $server);
						break;

					case 'get' :
						$res = get("http://minecraft-api.com/v1/get/?server=" . $server);
						break;

					case 'players' :
						$res = get("http://minecraft-api.com/v1/players/?server=" . $server);
						break;

					case 'motd' :
						$res = get("http://minecraft-api.com/v1/motd/?server=" . $server);
						break;

					case 'batched' :
						$res = get("http://minecraft-api.com/v1/batched/?server=" . $server);
						break;

					case 'logo' :
						$res = get("http://minecraft-api.com/v1/logo/?server=" . $server);
						break;

					case 'query' :
						$res = get("http://minecraft-api.com/v1/query/?server=" . $server);
						break;

					default :
						$res = "null";
						break;
				}

				$result = json_decode($res, true);
				return $result;

			} else {
				return "null";
			}
		} else {
			return "null";
		}
	}

}
?>