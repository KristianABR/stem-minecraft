<?php
if (isset($_GET['id'])) {
	//user::find($_GET['id']);

	$db -> where('id', $_GET['id']);
	$results = $db -> get('users');

	if (isset($results[0])) {
		echo "<div class='profile box'>
	<div class='profileheader'>
		<div class='profile-pic'>
			<img width='140px' src='/face/".$results[0]['minecraftaccount'].".png' />
		</div>
		<strong class='profile-user'>" . $results[0]['username'] . "</strong>
	</div>
	<div class='profile-nav'>
		<dl data-tab>
			<dd>
				<a href='#biography' class='profile-icon'><i class='fa fa-user'></i> Profil</a>
			</dd>
			<dd>
				<a href='#servers' class='servers-icon'><i class='fa fa-tasks'></i> Servere</a>
			</dd>
			<dd>
				<a href='#pictures'><i class='fa fa-camera'></i> Billeder</a>
			</dd>
			<dd>
				<a href='#favorites'><i class='fa fa-plus'></i> Favoritter</i></a>
			</dd>
		</dl>";
		if ($loggedIn && $results[0]["id"] == $_SESSION["id"]) {
			echo "
					<a href='edit' class='float-right'><i class='fa fa-gear'></i></a>
				";
		}

		echo "</div>
	<div class='profile-biography'>
		<div class='tabs-content'>
			<div class='content active' id='biography'>
				<h3>Om mig</h3>
				<p>";
		echo(isset($results[0]['bio'])) ? $results[0]['bio'] : 'Denne bruger har endnu ikke sat en beskrivelse.';
		echo "</p>
			</div>
			<div class='content' id='servers'>
				<h3>Brugerens tilføjede servere</h3>";
		$server = new server();
		$server -> find($results[0]['username'], 1);
		
		
		echo "</div>
			<div class='content' id='pictures'>
				<h3>Billeder</h3>
				<p>
					Denne bruger har ikke tilføjet nogle billeder til sit galleri...
				</p>
			</div>
			<div class='content' id='favorites'>
				<h3>Favoritter</h3>";
		if (!empty($results[0]['favorite'])) {
			$servers = explode(',', $results[0]['favorite']);

			$serv = array();
			foreach ($servers as $server => $value) {
				$serv[] = server::find((int)$value);

			}

			$processed = array();
			if (is_array($serv)) {
				foreach ($serv as $subarr) {
					if (is_array($subarr)) {
						foreach ($subarr as $id => $value) {
							if (!isset($processed[$id])) {
								$processed[$id] = array();
							}

							$processed[$id][] = $value;
						}
					}
				}
			}

			server::show($processed[0], 1);
		} else {
			echo $results[0]['username'] . ' har ikke tilføjet nogle servere til sine favoritter.';
		}
		echo "
			</div>
		</div>
	</div>
</div>

<div id='settings' class='reveal-modal medium' data-reveal>
	<h2>Skift banner</h2>
	<p>
		Banneret er noget af det første man ser, når man kommer ind på profilen - brug derfor et godt billede!
	</p>
	<form>
		<div class='row collapse'>
			<div class='small-3 large-2 columns'>
				<span class='prefix'>http://</span>
			</div>
			<div class='small-9 large-10 columns'>
				<input type='text' placeholder='Indtast link til billede'>
			</div>
		</div>
	</form>
	<a class='close-reveal-modal'>&#215;</a>
</div>";
	} else {
		echo '<div class="profile box">
    <div class="profileheader">
        <div class="profile-pic">
            <a href=""> <img src="https://minotar.net/helm/steve/140" alt="" /> </a>
        </div>
        <strong class="profile-user">Steve</strong>
    </div>
    <div class="profile-nav">
        <dl data-tab>
            <dd>
                <a href="#biography" class="profile-icon"><i class="fa fa-user"></i> Profil</a>
            </dd>
        </dl>
    </div>
    <div class="profile-biography">
        <div class="tabs-content">
            <div class="content active" id="biography">
                <h3>Om mig</h3>
                <p>
                    Steve is a Main Character or Protagonist in Minecraft. Very little is known about Steve and who or what it is, where it is from, what its goal is, and if it\'s even human.
                </p>

                <h5>Personality</h5>
                <p>
                    Steve doesn\'t have much of a personality nor a real goal on its own. It never talks, all it really does is actions related to the players choice.
                </p>

                <h5>Powers and Abilities</h5>
                <p>
                    Steve is capable of carrying over four hundred thousand tons of materials, running fast, fighting hordes of various monsters, and it never has to sleep. Steve is also capable of building huge creations with no help at all. It can also build portals, use alchemy, and shoot arrows far.
                </p>

                <h5>Story</h5>
                <p>
                    Since the common theorized story of Minecraft is that Steve is trying to save the world and Testificates from the Endermen army and their leader the Dragon, it can be based on that Steve is caring and brave. However; since its personality is based on you, its up to you to decide what it does and how it acts. Is Steve going to risk everything for the Testificates, is it going to procrastinate and save the world later, is it going to be a morally ambigious anti hero who constantly steals from the Testificates and makes it rather unclear whether its doing it for the greater good or just its own selfish gain, or is it not even going to be the slightest bit heroic and just run around mindlessly killing animals and testificates not even trying to save the world? Another factor is what mode you pick, if you pick Hardcore mode and fail that ultimately means that Steve failed its quest to save the world and that the Ender-Dragon succeeds in whatever his goal was, however; if you chose survival mode and died you simply come back without your tools.
                </p>

                <h5>Appearance</h5>
                <p>
                    Steves default clothes are blue pants and a shirt, though, with editing you can change its clothes and apperance completely.
                </p>
            </div>
        </div>
    </div>
</div>';
	}
} else {
	if ($loggedIn) {

		header("Location: /profile/" . $_SESSION['id']);

	} else {
		echo 'Du er ikke logged ind.';
	}
}
?>