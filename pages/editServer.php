<?php
if (!empty($_POST)) {
	if ($loggedIn) {

		$x = server::find((int)$_GET['id']);
		if (isset($x)) {
			if ($x[0]['username'] === $_SESSION['username'] || $isAdmin) {
				include DIR_3RDPARTY . 'MinecraftQuery.class.php';

				$server = server::query($_POST['ip'].':'.$_POST['port'], 'get');

				if (!isset($server['error'])) {

					$data = array();

					foreach ($_POST as $key => $d) {
						if ($key === "submit" || $key === "a") {

						} else {
							$data[$key] = $d;

							$db -> where('id', $_GET['id']);
							$db -> update('servers', $data);

							header("Location: /server/" . $_GET['id']);
						}

					}
				}else{
					echo $server['error'];
				}
			} else {
				echo 'Du ejer ikke serveren.';
			}
		}
	} else {
		echo 'Du skal være logget ind!';
	}

} else if (isset($_GET['id'])) {
	echo '<h2>Ændre server information</h2>';
	if ($loggedIn) {

		$x = server::find((int)$_GET['id']);
		if (isset($x)) {
			if ($x[0]['username'] === $_SESSION['username']  || $isAdmin) {
				echo '
    <form role="form" method="post" action="/server/edit/' . $_GET['id'] . '">
            <div class="form-group">
                    <label for="name">Navn</label>
                    <input type="text" class="form-control" name="name" value="' . $x[0]["name"] . '">
            </div>

            <div class="form-group">
                    <label for="ip">IP</label>
                    <input type="text" class="form-control" name="ip" value="' . $x[0]["ip"] . '">
            </div>

            <div class="form-group">
                    <label for="port">Port</label>
                    <input type="text" class="form-control" name="port" value="' . $x[0]["port"] . '">
            </div>

            <div class="form-group">
                    <label for="description">Beskrivelse</label>
                    <textarea class="form-control" name="description">' . $x[0]["description"] . '</textarea>
            </div>

            <div class="form-group">
                    <label for="website">Evt. Hjemmeside</label>
                    <input type="text" class="form-control" name="website" value="' . $x[0]["website"] . '">
            </div>

            <div class="form-group">
                    <label for="categories">Categories goin here</label>
                    <input type="text" class="form-control" name="categories" value="' . $x[0]['categories'] . '">
            </div>

            <div class="form-group">
                    <label for="votifier">Votifier</label>
                    <input type="text" class="form-control" name="votifier" value="' . $x[0]["votifier"] . '">
            </div>

            <div class="form-group">
                    <label for="votifierPublicKey">Votifier Public Key</label>
                    <textarea class="form-control" name="votifierPublicKey">' . $x[0]["votifierPublicKey"] . '</textarea>
            </div>

            <div class="form-group">
                    <label for="votifierPort">Votifier Port</label>
                    <input type="text" class="form-control" name="votifierPort" value="' . $x[0]["votifierPort"] . '">
            </div>

            <div class="form-group">
                    <label for="showPlugins">Show Plugins?</label>
                    <input type="text" class="form-control" name="showPlugins" value="' . $x[0]["showPlugins"] . '">
            </div>
    
            <input class="button small" type="submit" name="submit" value="Ændre">
            <input class="button small alert" type="submit" name="delete" value="Slet Server">
    </form>
	';
			} else {
				echo 'Du ejer ikke denne server.';
			}
		}

	} else {
		echo 'Du skal være logget ind for at ændre i servere!';
	}
} else {
	echo 'Der skete en fejl. Du har ikke sat noget id.';
}
?>
