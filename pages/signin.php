<?php
if (!defined('GD')) die('This file cannot be accessed directly');
    if ($loggedIn) {
      echo 'Du er allerede logget ind.';
    } else {
        echo '
        	<div class="small-12 medium-6 signupform columns">
		<h3 class="text-center">Login</h3>
		<form role="form" method="post" action="'.$_SERVER["PHP_SELF"].'?page=signinDo">
		<div class="small-12 columns">
			<label for="username">Brugernavn</label>
            <input type="text" class="form-control" name="username" required>
        </div>
        
        <div class="small-12 columns">
            <label for="passwd">Password</label>
            <input type="password" class="form-control" name="password" required>
        </div>
        <div class="columns">
       		<input class="button small radius" type="submit" name="submit" value="Login">
       		 <a href="'.$_SERVER['PHP_SELF'].'?page=recovery">Gendan Bruger</a>
       	</div>
		</form>
	</div>
<div class="small-12 medium-6 signupform columns">
		<h3 class="text-center">Tilmeld</h3>
		<div class="alert alert-success radius" id="alert" style="display: none">
        	<p>Der er blevet sendt en mail med informationer om, hvordan du logger ind.</p>
		</div>

<form role="form" method="post" action="'.$_SERVER["PHP_SELF"].'?page=signupDo">
             <div class="small-12 large-6 columns">
	            <label for="username">Brugernavn</label>
	            <input type="text" name="username" required>
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="mcAcc">Minecraft Account Name</label>
	            <input type="text" name="mcAcc">
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="email">Email</label>
	            <input type="text" name="email" required>
	        </div>
	        <div class="small-12 large-6 columns">
	            <label for="confEmail">Bekræft Email</label>
	            <input type="text" name="confEmail" required>
	        </div>
	        <div class="columns">
        		<input class="button small radius" type="submit" name="submit" value="Tilmeld">
        	</div>
        </form>
</div>
        ';
    }

?>