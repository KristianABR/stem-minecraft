<?php
if (!defined('GD'))
  die('This file cannot be accessed directly');
?>
<div class="contain-to-grid">
  <nav class="top-bar" data-topbar>
    <ul class="title-area">
      <li class="name">
      <h1><a href=<?php echo DOMAIN . "index.php"; ?>><?php echo CONFIG::site_name?></a></h1>
    </li>
    <li class="toggle-topbar menu-icon">
      <a href="#"><span>Menu</span></a>
    </li>
  </ul>

  <section class="top-bar-section">
      <ul class="left">
        <li class="has-form">
        <form method="POST" action="/search/">
          <div class="row collapse">
            <div class="large-8 small-9 columns">
            <?php
              $value = (isset($_GET['q'])) ? 'value="'.$_GET['q'].'"' : '' ;
            ?>
              <input type="text" name="search" placeholder="Søg..." <?php echo $value ?>>
            </div>
            <div class="large-4 small-3 columns">
              <input type="submit" class="success button rightradius expand" value="Find">
            </div>
            
          </div>
          </form>
        </li>
      </ul>
    <ul class="right">
      <li <?php
      if (!isset($_GET['page'])) {echo 'class="active"';
      }
      ?>>
        <a href=<?php echo DOMAIN . "index.php"; ?>><i class="fa fa-home"></i> Hjem</a>
      </li>

      <?php
      if ($loggedIn) {
        echo '<li ' . active('profile') . '><a href="/profile/"><i class="fa fa-user"></i> Profil</a></li>
            <li '.active('addServer').'><a href="/server/add/"><i class="fa fa-plus"></i> Tilføj Server</a></li>';

      } else {

        echo '  <li ' . active('signin') . '><a href="#" data-reveal-id="login"><i class="fa fa-sign-in"></i> Log ind</a></li>';
      }
      ?>
      <?php
      if ($loggedIn) {
        echo '<li ' . active('logout') . '><a href="/logout/"><i class="fa fa-sign-out"></i> Log Ud</a></li>';
      }
      ?>
    </ul>
  </section>
</nav>
</div>

<div id="login" class="reveal-modal" data-reveal>
<div class="small-12 medium-6 signupform columns">
    <h3 class="text-center">Login</h3>
    <form role="form" method="post" action="/signin/">
    <div class="small-12 columns">
      <label for="username">Brugernavn</label>
            <input type="text" class="form-control" name="username" required>
        </div>
        
        <div class="small-12 columns">
            <label for="passwd">Password</label>
            <input type="password" class="form-control" name="password" required>
        </div>
        <div class="columns">
          <input class="button small radius" type="submit" name="submit" value="Login">
        </div>
    </form>
  </div>
<div class="small-12 medium-6 signupform columns">
    <h3 class="text-center">Tilmeld</h3>
    <div class="alert alert-success radius" id="alert" style="display: none">
          <p>Der er blevet sendt en mail med informationer om, hvordan du logger ind.</p>
    </div>

<form role="form" method="post" action="/signup/">
             <div class="small-12 large-6 columns">
              <label for="username">Brugernavn</label>
              <input type="text" name="username" required>
          </div>
          <div class="small-12 large-6 columns">
              <label for="mcAcc">Minecraft Account Name</label>
              <input type="text" name="mcAcc">
          </div>
          <div class="small-12 large-6 columns">
              <label for="email">Email</label>
              <input type="text" name="email" required>
          </div>
          <div class="small-12 large-6 columns">
              <label for="confEmail">Bekræft Email</label>
              <input type="text" name="confEmail" required>
          </div>
          <div class="columns">
            <input class="button small radius" type="submit" name="submit" value="Tilmeld">
          </div>
        </form>
</div>
  <a class="close-reveal-modal">&#215;</a>
</div>
<div class="large-12 small-10 row">